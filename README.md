# Oracle Service

# Commands

## Usage

```bash
USAGE: zen-oracle.exe [--help] [<subcommand> [<options>]]

SUBCOMMANDS:

    commit, c <options>   commit an item or a data set
    query, q <options>    query for committed data
    attest, a <options>   attest on committed data
    audit, p <options>    get the audit path for a committed item
    server, s <options>   create a web server

    Use 'zen-oracle.exe <subcommand> --help' for additional information.

OPTIONS:

    fields                list output fields
    --help                display this list of options.
```

## Environmental Variables

1. Make sure all the environment variables are provided and correct
    - `zen_path` : path of the oracle committer
    - `zen_wallet_password`: password of the f# wallet
    - `zen_node_uri`: uri of the node
    - `oracle_api`: `uri:port` api port for oracle requests
    - `mongo_connection`: MongoDB connection string (optional. defaults to `"mongodb://127.0.0.1:27017"`)

## Commit

To commit use the command:

```bash
zen-oracle.exe commit [--timestamp <timestamp>] [--notx] [--stdin] [--field <field>] [<filename>]
```

or the abbreviation:

```bash
zen-oracle.exe c [--timestamp <timestamp>] [--notx] [--stdin] [--field <field>] [<filename>]
```

The `commit` command has a parameter `<filename>` and the following flags:

- **`--timestamp`**, **`-t`** `<timestamp>`
Time of the committed data (in milliseconds since the Unix epoch - 00:00:00 UTC on 1 January 1970).
- **`--notx`**, **`-x`**

    Don't create a commitment transaction, just return the message body.

- **`--stdin`**, **`-i`**

    When this flag is used - get JSON string from standard input.

- **`--field`**, **`-f`** `<field>`

    Output only the specified field.

For the `<filename>` parameter use the name of a JSON data file you want to commit.

The JSON file should be a record with names and values, like this:

```json
{ "APPL" : 10 , "ABCD" : 12 , "WXYZ" : 123 , "XYXY" : 6456 }
```

## Attest

To attest use the command:

```bash
zen-oracle.exe attest [--root <root>] [--timestamp <timestamp>] [--commit <commit>] [--publickey <pk>] [--contract <cid>] [--notx] [--field <field>]
```

or the abbreviation:

```bash
zen-oracle.exe a [--root <root>] [--timestamp <timestamp>] [--commit <commit>] [--publickey <pk>] [--contract <cid>] [--notx] [--field <field>]
```

The `attest` command has the following flags:

- **`--root`**, **`-r`** `<root>`

    Root of the committed Merkle tree.

- **`--timestamp`**, **`-t`** `<timestamp>`
Time of the committed data (in milliseconds since the Unix epoch - 00:00:00 UTC on 1 January 1970).
- **`--commit`**, **`-c`** `<commit>`

    Commit ID of the committed data.

- **`--publickey`**, **`-p`** `<pk>`

    Public key of the recipient address.

- **`--contract`**, **`-d`** `<cid>`

    Contract ID of the recipient contract.

- **`--notx`**, **`-x`**

    Don't create an attestation transaction, just return the message body.

- **`--field`**, **`-f`** `<field>`

    Output only the specified field.

You have to use either **`--commit`** or both **`--root`** and **`--timestamp`**.

You can only have at most 1 recipient (either **`publickey`** or **`contract`**).

If you don't provide a recipient the attestation token will be sent to the sender.

## Query

To query use the command:

```bash
zen-oracle.exe query [subcommand]
```

or the abbreviation:

```bash
zen-oracle.exe q [subcommand]
```

There are 2 things you can query for - the **oracle public key** or information about **committed data**.

### Oracle Public Key

To get the public key of the oracle run:

```bash
zen-oracle.exe query publickey
```

or the abbreviation:

```bash
zen-oracle.exe q p
```

### Committed Data

To get information about committed data by time bounds or key, run:

```bash
zen-oracle.exe query timebounds [--low <low>] [--high <high>] [--key <key>] [--skip <n>] [--take <n>] [count]
```

or the abbreviation:

```bash
zen-oracle.exe q t [--low <low>] [--high <high>] [--key <key>] [--skip <n>] [--take <n>] [count]
```

The `query timebounds`  command has the following flags:

- **`--low`**, **`-l`** `<low>`

    Lower time bound.

- **`--high`**, **`-h`** `<high>`

    Upper time bound.

- **`--key`**, **`-k`** `<key>`

    Key to search the value for.

- **`--skip`**, **`-s`** `<n>`

    Skip the first `<n>` items.

- **`--take`**, **`-t`** `<n>`

    Take only the first `<n>` items (after the skip if there is one).

- **`count`**, **`c`**

    Return the total amount of items satisfying the query.

It will provide you information about all the values committed by the server for the given key within the given time bounds.

If no key is provided it will provide information about all the values within the time bounds regardless of keys.

If no time bounds are provided it will provide information about all the values committed by the server for the given keys, regardless of time bounds.

To get information about committed values for a specific list of items, run:

```bash
zen-oracle.exe q k [--stdin] <filename>
```

The `query timekeyvalue`  command has a parameter `<filename>` and the following flags:

- **`--stdin`**, **`-i`**

    When this flag is used - get JSON string from standard input.

This command takes a JSON file which is a list where each item has the following parameters:

- `**key**`

    The name of the item.

- `**low**`

    The earliest time for the item to appear.

- `**high**`

    The latest time for the item to appear (**optional** - if it's not given it's equal to `low`).

In return you'll get the same list where each item has it's committed value added to it in the `value` field, and items which weren't committed in the given time bounds are removed.

## Audit Path

To get an audit path in the Merkle tree of a committed data set use the command:

```bash
zen-oracle.exe audit [--commit <commit>] [--root <root>] [--stdin] [<item>]
```

or the abbreviation:

```bash
zen-oracle.exe p [--commit <commit>] [--root <root>] [--stdin] [<item>]
```

The `audit` command has a parameter `<item>` and the following flags:

- **`--commit`**, **`-c`** `<commit>`

    Commit hash.

- **`--root`**, **`-r`** `<root>`

    Root hash.

- **`--stdin`**, **`-i`**

    When this flag is used - get item's JSON string from standard input.

For the `<item>` parameter use the name of a JSON file which contains items for which you want to get audit paths in the given commit.

The JSON file should be a record with names and values, like this:

```json
{ "APPL" : 10 , "ABCD" : 12 , "WXYZ" : 123 , "XYXY" : 6456 }
```

## Server

To run the server use the command:

```bash
zen-oracle.exe server
```

or the abbreviation:

```bash
zen-oracle.exe s
```

The server will run on a port specified with the `**zen_api**` environment variable.

The `server` command supports the following flags:

- `**--bind**`, `**-b**` `<address>` 
API port
- `**--chain**`, **`-c`** `<chain>`
Node chain
- `**--origin**`, `**-o**` `<origin>`
CORS origin
- `**--maxtake**`, `**-l**` `<n>`
Maximum size of take (default: `1000`)
- `**--maxbodysize**`, `**-s**` `<n>` Maximum size of body for the `getValues` endpoint (default: `1000`)

The server has the following endpoints:

### `attest`

Use the `attest` endpoint to ask for an attestation message body.

This **GET** request has the following parameters:

- **`root`**

    Root of the committed Merkle tree.

- **`timestamp`**
Time of the committed data (in milliseconds since the Unix epoch - 00:00:00 UTC on 1 January 1970).
- **`commit`**

    Commit ID of the committed data.

- **`pk`**

    Public key of the recipient address.

- **`cid`**

    Contract ID of the recipient contract.

- **`address`**

    Address of the recipient (either a contract address or a public key hash address).

You have to use either **`commit`** or both **`root`** and **`timestamp`**.

You can only have at most 1 recipient (either `**pk**` or `**cid**`).

If you don't provide a recipient the attestation token will be sent to the sender.

### `query`

Use the `query` URI endpoint to check if a commitment is provided by the oracle service and get a list of items committed by the service.

This **GET** request has the following parameters:

- **`low`**

    Lower time bound.

- **`high`**

    Upper time bound.

- **`key`**

    Key to search the value for.

- `**skip**`

    Skip the first `<n>` items.

- `**take` (mandatory)**

    Take only the first `<n>` items (after the skip if there is one).

### `count`

Use the `count` URI endpoint to get the number of items committed by the service.

This **GET** request has the following parameters:

- **`low`**

    Lower time bound.

- **`high`**

    Upper time bound.

- **`key`**

    Key to search the value for.

### `getValues`

Use the `getValues` URI endpoint to get the values of a list of items.

This **POST** request takes a JSON body which is a list where each item has the following parameters:

- `**key**`

    The name of the item.

- `**low**`

    The earliest time for the item to appear.

- `**high**`

    The latest time for the item to appear (**optional** - if it's not given it's equal to `low`).

In return you'll get the same list where each item has it's committed value added to it in the `value` field, and items which weren't committed in the given time bounds are removed.
