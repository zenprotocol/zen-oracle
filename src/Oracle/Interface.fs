module Interface

open Consensus.Crypto
open Infrastructure
open Consensus
open Wallet

open Error
open FSharp.Data
open ProgramMonad
open Merkle
open Record



type Return = {
        root               : string
        timestamp          : uint64
        commit             : string
        tx                 : string option
        messageBody        : JsonValue
        messageBodyEncoded : string
    } with
    member self.toJson =
        [|
            "root"               , self.root |> JsonValue.String
            "timestamp"          , self.timestamp |> string |> JsonValue.String
            "commit"             , self.commit |> JsonValue.String
            "tx"                 , self.tx |> function | Some tx -> JsonValue.String tx | None -> JsonValue.Null
            "messageBody"        , self.messageBody
            "messageBodyEncoded" , self.messageBodyEncoded |> JsonValue.String
        |]
        |> JsonValue.Record
        |> Api.Helpers.omitNullFields



let encodeMessageBody (msgBody : Zen.Types.Data.data) : string  =
    msgBody
    |> Consensus.Serialization.Data.serialize
    |> FsBech32.Base16.encode

let parseHash s =
    Hash.fromString s
    |> withError (Error.Data Error.InvalidHash)

let getField (field : string) (record : Return) : Program<string> =
    typeof<Return>.GetProperties()
    |> Array.tryFind (fun item -> item.Name = field)
    |> Option.map (fun item -> string (item.GetValue record))
    |> ofOption (Error.Argument <| Error.InvalidFieldName field)



module Commit =
    
    let getOraclePubKey : Program<PublicKey> =
        WalletActions.getPublicKey.handle
        >>= (PublicKey.fromString >> withError (Error.Data Error.InvalidPK))
    
    let createAndSendTransaction (msgBodyEnc : string) : Program<string> =
        let env : WalletActions.Execute.Env = {
            address       = Contract.Address
            command       = Contract.Command.Commit
            messageBody   = msgBodyEnc
            returnAddress = false
        }
        
        WalletActions.Execute.handle env
    
    let commitToDatabase (pk : string) (commit : DataAccess.Commit.T) : Program<unit> =
        program {
            let! config =
                ask
            
            let! db =
                DataAccess.connectToDatabase config None
                |> withError Error.DatabaseDisconnected
            DataAccess.Collections.PublicKey.put db pk
            DataAccess.Commit.put db commit
        }
    
    let createCommit (timestamp : uint64) (items : Map<string, Item.Item * Proof>) (tree : Merkle.Merkle) : DataAccess.Commit.T =
        {
            commitId  = Hash.commit tree.hash timestamp
            timestamp = timestamp
            root      = tree.hash
            items     = items
        }
    
    let handle (json : string) (timestamp : uint64) (createTransaction : bool) : Program<Return> =
        program {
            
            let! config =
                ask
            
            let! oraclePubKey =
                getOraclePubKey
            
            let! data =
                JSON.parse json
            
            if Map.isEmpty data then
                return! ofResult <| Error EmptyJSON
            
            let data_with_hashes =
                data
                |> Map.map (fun name item -> (item , Map.find name <| Hash.record data))
            
            let sorted_data_with_hashes =
                Record.sort data_with_hashes
            
            let tree =
                 sorted_data_with_hashes
                 |> Array.map (snd >> snd)
                 |> Merkle.computeMerkleTree
            
            let data_with_proofs =
                tree
                |> Merkle.AuditPath.generate
                |> Array.zip sorted_data_with_hashes
                |> Array.mapi (fun i ((name , (item, _)) , path) -> (name , (item , { index = i ; path = path })))
                |> Map.ofArray
            
            let commitHash =
                Hash.commit tree.hash timestamp
            
            let msgBody =
                MessageBody.Data.create commitHash None None
            
            let msgBodyEnc =
                encodeMessageBody msgBody
            
            let! txHash =
                if createTransaction then
                    createAndSendTransaction msgBodyEnc 
                    |@> Some
                else
                    ret None
            
            return!
                createCommit timestamp data_with_proofs tree
                |> commitToDatabase (PublicKey.toString oraclePubKey)
            
            return {
                root               = tree.hash.ToString()
                timestamp          = timestamp
                commit             = commitHash.ToString()
                tx                 = txHash
                messageBody        = MessageBody.Data.toJson config.chain msgBody
                messageBodyEncoded = msgBodyEnc
            }
                
        }



module Query =
    
    module PublicKey =
        
        let handle : Program<string> =
            program {
                let! config =
                    ask
                
                let! db =
                    DataAccess.connectToDatabase config None
                    |> withError Error.DatabaseDisconnected
                
                return!
                    match DataAccess.Collections.PublicKey.tryGet db with
                    | Some pk ->
                        ret pk
                    | None ->
                        WalletActions.getPublicKey.handle
            }
    
    
    module TimeBounds =
        
        open System
        
        type Return = {
                commitId  : Hash
                timestamp : uint64
                root      : Hash
                name      : string
                hash      : Hash
                value     : Item.Item
                path      : Hash list
                index     : int
        } with
            member self.toJson =
                [|
                    "commitId"  , self.commitId |> Hash.toString |> JsonValue.String
                    "timestamp" , self.timestamp |> string |> JsonValue.String
                    "root"      , self.root |> Hash.toString |> JsonValue.String
                    "name"      , self.name |> JsonValue.String
                    "hash"      , self.hash |> Hash.toString |> JsonValue.String
                    "value"     , self.value |> JSON.Item.toJson
                    "path"      , self.path |> List.map (Hash.toString >> JsonValue.String) |> List.toArray |> JsonValue.Array
                    "index"     , self.index |> string |> JsonValue.String
                |]
                |> JsonValue.Record
        
        let private toNullable (x : 'a option) : Nullable<'a> =
            x
            |> Option.map Nullable
            |> Option.defaultValue (Nullable())
        
        let handle (db:MongoDB.Driver.IMongoDatabase) (skip : int option) (take : int option) (low : uint64 option) (high : uint64 option) (name : string option) : Return list =
            
            let l =
                Option.defaultValue 0UL low
            
            let h =
                Option.defaultValue (uint64 System.Int64.MaxValue) high
            
            let s =
                toNullable skip
            
            let t =
                toNullable take
            
            let itemToReturn (item : DataAccess.Collections.Item.T) =
                
                {
                    commitId  = item.commitId
                    timestamp = item.timestamp
                    root      = item.root
                    name      = item.name
                    hash      = Hash.namedItem item.name item.item
                    value     = item.item
                    path      = item.proof.path |> Array.toList
                    index     = item.proof.index
                }
            
            name
            |> Option.map (DataAccess.Collections.Item.getByTimeBoundsAndName db s t l h)
            |> Option.defaultWith (fun () -> DataAccess.Collections.Item.getByTimeBounds db s t l h)
            |> Seq.map itemToReturn
            |> Seq.toList
    
    
    module TimeBoundsCount =
        
        let handle (db:MongoDB.Driver.IMongoDatabase) (low : uint64 option) (high : uint64 option) (name : string option) : int64 =
            
            let l =
                Option.defaultValue 0UL low
            
            let h =
                Option.defaultValue (uint64 System.Int64.MaxValue) high
            
            name
            |> Option.map (DataAccess.Collections.Item.countByTimeBoundsAndName db l h)
            |> Option.defaultWith (fun () -> DataAccess.Collections.Item.countByTimeBounds db l h)
    
    
    module TimeKeyValue =
        
        open JSON.ParseType.Request
        
        type Item =
            {
                key  : string
                low  : uint64
                high : uint64 option
            }
        
        type Return =
            {
                key   : string
                low   : uint64
                high  : uint64 option
                value : Item.Item
            } with
                member self.toJson =
                    [|
                        "key"   , self.key |> JsonValue.String |> Some
                        "low"   , self.low |> string |> JsonValue.String |> Some
                        "high"  , self.high |> Option.map (string >> JsonValue.String)
                        "value" , self.value |> JSON.Item.toJson |> Some
                    |]
                    |> Array.map (fun (key , oval) -> oval |> Option.map (fun value -> (key , value)))
                    |> Array.choose id
                    |> JsonValue.Record
        
        let rec private distinctSortedAux acc body =
            match body with
            | [] ->
                acc
            | x :: [] ->
                x :: acc
            | x :: y :: xs ->
                if x = y then
                    distinctSortedAux acc (x :: xs)
                else
                    distinctSortedAux (x :: acc) (y :: xs) 
        
        let private distinctSorted =
            distinctSortedAux []
        
        let distinct (body : Item list) : Item list =
            body
            |> List.sort
            |> distinctSorted
        
        let parseItem (q : TimeKeyValueBodyJson.Root) : Item =
            { key = q.Key ; low = uint64 q.Low ; high = Option.map uint64 q.High }
        
        let parseJson (body : string) : Item list =
            TimeKeyValueBodyJson.Parse body
            |> Array.map parseItem
            |> Array.toList
        
        let handleItem (db:MongoDB.Driver.IMongoDatabase) (item : Item) : Return option =
            FSharpx.Option.maybe {
                
                let h =
                    Option.defaultValue item.low item.high
                
                let! res =
                    TimeBounds.handle db None None (Some item.low) (Some h) (Some item.key)
                    |> List.tryHead
                
                return {
                    key   = res.name
                    low   = item.low
                    high  = item.high
                    value = res.value
                }
            }
        
        let handle (db:MongoDB.Driver.IMongoDatabase) (items : Item list) : Program<Return list> =
            program {
                let! config = ask
                if uint64 (List.length items) > config.bodySizeLimit then
                    return! ofResult <| Error (Data ListTooBig)
                else
                    return
                        items
                        |> distinct
                        |> List.choose (handleItem db)
            }
    
    type Return =
        | QPublicKey       of string
        | QTimeBounds      of TimeBounds.Return list
        | QTimeBoundsCount of int64 
        | QTimeKeyValue    of TimeKeyValue.Return list
        with member self.toJson =
                match self with
                | QPublicKey pk ->
                    JsonValue.String pk
                | QTimeBounds results ->
                    results
                    |> List.map (fun res -> res.toJson)
                    |> List.toArray
                    |> JsonValue.Array
                | QTimeBoundsCount count ->
                    JsonValue.Number (decimal count)
                | QTimeKeyValue results ->
                    results
                    |> List.map (fun res -> res.toJson)
                    |> List.toArray
                    |> JsonValue.Array 
                    



module Clear =
    
    let handle : Program<unit> =
        program {
            let! config =
                ask
            
            let! db =
                DataAccess.connectToDatabase config None
                |> withError Error.DatabaseDisconnected
            
            DataAccess.dropAll db
        }



module Attest =
    
    open Zen.Types.Extracted
    
    type Recipient =
        | PK      of string
        | CID     of string
        | Address of string
    
    let createAndSendTransaction (msgBodyEnc : string) : Program<string> =
        
        let env : WalletActions.Execute.Env = {
            address       = Contract.Address
            command       = Contract.Command.Attest
            messageBody   = msgBodyEnc
            returnAddress = false
        }
        
        WalletActions.Execute.handle env
    
    let getOraclePubKey db =
        DataAccess.Collections.PublicKey.get db
        |> PublicKey.fromString
        |> withError (Error.Data Error.InvalidPK)
    
    let recipientToLock (recip : Recipient) : Program<lock> =
        program {
            let! config = ask
            match recip with
            | PK pk ->
                return!
                    PublicKey.fromString pk
                    |> withError (Error.Data Error.InvalidPK)
                    |@> PublicKey.hash
                    |@> Hash.bytes
                    |@> PKLock
            | CID cid ->
                return!
                    FsBech32.Base16.decode cid
                    |> Option.bind ContractId.fromBytes
                    |> withError (Error.Data Error.InvalidCID)
                    |@> ZFStar.fsToFstContractId
                    |@> ContractLock
            | Address address ->
                match Wallet.Address.decodeAny config.chain address with
                | Ok (Address.Contract contractId) ->
                    return
                        contractId
                        |> ZFStar.fsToFstContractId
                        |> ContractLock
                | Ok (Address.PK pkHash) ->
                    return
                        pkHash
                        |> Hash.bytes
                        |> PKLock
                | Error _ ->
                    return!
                        Error (Error.Data Error.InvalidAddress)
                        |> ofResult
        }

    
    module ByCommit = 
        
        let handle (db:MongoDB.Driver.IMongoDatabase) (commit : string) (recipient : Recipient option) (createTransaction : bool) : Program<Return> =
            program {
                
                let! config =
                    ask
                
                let! commitId =
                    parseHash commit
                
                let! pk =
                    getOraclePubKey db
                
                let! recip =
                    match recipient with
                    | Some recip ->
                        recipientToLock recip
                        |@> Some
                    | None ->
                        ret None
                
                let msgBody =
                    MessageBody.Data.create commitId recip (Some pk)
                
                let msgBodyEnc =
                    encodeMessageBody msgBody
                
                let! txHash =
                    if createTransaction then
                        createAndSendTransaction msgBodyEnc
                        |@> Some
                    else
                        ret None
                
                let! commit =
                    DataAccess.Collections.Commit.tryGet db commitId
                    |> withError (Error.NotFound Error.Commit)
                
                let root =
                    commit.root
                
                let timestamp =
                    commit.timestamp
                
                return {
                    root =
                        root.ToString()
                    timestamp =
                        timestamp
                    commit =
                        commitId.ToString()
                    tx =
                        txHash
                    messageBody =
                        MessageBody.Data.toJson config.chain msgBody
                    messageBodyEncoded =
                        msgBodyEnc
                }
            }
    
    module ByRootAndTimestamp =
        
        let handle (db:MongoDB.Driver.IMongoDatabase) (root : string) (timestamp : uint64) (recipient : Recipient option) (createTransaction : bool) : Program<Return> =
            program {
                
                let! config =
                    ask
                
                let! rootHash =
                    parseHash root
                
                let commitHash =
                    Hash.commit rootHash timestamp
                
                let! pk =
                    getOraclePubKey db
                
                let! recip =
                    match recipient with
                    | Some recip ->
                        recipientToLock recip
                        |@> Some
                    | None ->
                        ret None
                
                let msgBody =
                    MessageBody.Data.create commitHash recip (Some pk)
                
                let msgBodyEnc =
                    encodeMessageBody msgBody
                
                let! txHash =
                    if createTransaction then
                        createAndSendTransaction msgBodyEnc
                        |@> Some
                    else
                        ret None
                
                let! timeRootCommit =
                    DataAccess.Collections.TimeRootCommit.tryGet db timestamp rootHash
                    |> withError (Error.NotFound Error.RootTimestamp)
                
                let commiId =
                    timeRootCommit.commitId
                
                return {
                    root =
                        root
                    timestamp =
                        timestamp
                    commit =
                        commiId.ToString()
                    tx =
                        txHash
                    messageBody =
                        MessageBody.Data.toJson config.chain msgBody
                    messageBodyEncoded =
                        msgBodyEnc
                }
            }


module Audit =
    
    type Proof = {
        name  : string
        index : int
        hash  : Hash
        path  : Hash list
    } with
        member self.toJson =
            [|
                "name"  , self.name |> JsonValue.String
                "index" , self.index |> string |> JsonValue.String
                "hash"  , self.hash |> Hash.toString |> JsonValue.String
                "path"  , self.path |> List.map (Hash.toString >> JsonValue.String) |> List.toArray |> JsonValue.Array
            |]
            |> JsonValue.Record
            
    
    type Return = {
        root   : string
        proofs : Proof list
    } with
        member self.toJson =
            [|
                "root"   , self.root |> JsonValue.String
                "proofs" , self.proofs |> List.map (fun p -> p.toJson) |> List.toArray |> JsonValue.Array
            |]
            |> JsonValue.Record
    
    module ByRoot =
        
        let handle (db:MongoDB.Driver.IMongoDatabase) (root : string) (record : Record) : Program<Return> =
            
            program {
                
                let! rootHash =
                    parseHash root
                
                if Seq.isEmpty <| DataAccess.Collections.TimeRootCommit.ByRoot.get db rootHash then
                    return! Error.NotFound Error.Root |> Error |> ofResult
                
                let items =
                    DataAccess.Collections.Item.getItems db rootHash record
                
                let getProof (item : DataAccess.Collections.Item.T) =
                    {
                        name =
                            item.name
                        index =
                            item.proof.index
                        hash =
                            Hash.namedItem item.name item.item
                        path =
                            Array.toList item.proof.path
                    }
                
                return {
                    root  =
                        root
                    proofs =
                        items
                        |> Seq.map getProof
                        |> Seq.toList
                }
            }
    
    module ByCommit =
        
        let handle (db:MongoDB.Driver.IMongoDatabase) (commit : string) (record : Record) : Program<Return> =
            program {
                
                let! commitId =
                    parseHash commit
                
                let! commit =
                    DataAccess.Collections.Commit.tryGet db commitId
                    |> withError (Error.NotFound Error.Commit)
                
                let items =
                    DataAccess.Collections.Item.getItems db commit.root record
                
                let getProof (item : DataAccess.Collections.Item.T) =
                    {
                        name =
                            item.name
                        index =
                            item.proof.index
                        hash =
                            Hash.namedItem item.name item.item
                        path =
                            item.proof.path |> Array.toList
                    }
                
                return {
                    root  =
                        commit.root.ToString()
                    proofs =
                        items
                        |> Seq.toList
                        |> List.map getProof
                }
            }
