module DataAccess

open MongoDB.Bson
open MongoDB.Driver
open MongoDB.FSharp

open Consensus



open Config
open Item
open Record



[<Literal>]
let DefaultConnectionString =
    "mongodb://127.0.0.1:27017"

[<Literal>]
let DbName =
    "oracle"


type timestamp = uint64

type Hash = Hash.Hash

type ArrayProof = {
    index : int
    path  : Hash []
}



let private connect (config : Config) : MongoClient =
    match config.envVars.mongoConn with
    | None ->
        MongoClient( DefaultConnectionString )
    | Some conn ->
        MongoClient( conn )
 
 /// Check if the mongodb server is running
let private isConnected (client : MongoClient) : bool =
    // An hack to get around the bad design of MongoDB.Driver
    let doc = BsonDocument.Parse( "{ping:1}" )
    client
        .GetDatabase( DbName )
        .RunCommandAsync( BsonDocumentCommand doc )
        .Wait(1000)

let private getDB (dbName : string option) (client : MongoClient) : IMongoDatabase option =
    if isConnected client then
        Some <| client.GetDatabase( dbName |> Option.defaultValue DbName )
    else
        None

let connectToDatabase (config : Config) (dbName : string option) : IMongoDatabase option =
    connect config |> getDB dbName



module Collections =
    
    
    module PublicKey =
        
        type T = {
            _id : BsonObjectId
            pk  : string
        }
        
        [<Literal>]
        let Collection =
            "PublicKey"
        
        let getCollection (db : IMongoDatabase) : IMongoCollection<T> =
            db.GetCollection<T>( Collection )
        
        let drop (db : IMongoDatabase) : unit =
            db.DropCollection( Collection )
        
        let put (db : IMongoDatabase) ( pk : string ) : unit =
            let collection = getCollection db 
            (collection.Find(Builders<T>.Filter.Empty)).ToEnumerable()
            |> Seq.tryHead
            |> Option.map (fun _ -> ())
            |> Option.defaultWith
                begin fun _ ->
                    {
                        _id = MongoDB.Bson.BsonObjectId(MongoDB.Bson.ObjectId.GenerateNewId())
                        pk  = pk
                    }
                    |> collection.InsertOne
                end
        
        let get (db : IMongoDatabase) : string =
            (getCollection db).Find(Builders<T>.Filter.Empty).Single().pk
        
        let tryGet (db : IMongoDatabase) : string option =
            try
                get db
                |> Some
            with _ ->
                None
        
        let count  (db : IMongoDatabase) : int64 =
            (getCollection db).CountDocuments(Builders<T>.Filter.Empty)
    
    
    module Item =
        
        open System
        
        type T = {
            _id       : BsonObjectId
            name      : string
            root      : Hash
            commitId  : Hash
            timestamp : timestamp
            item      : Item
            proof     : ArrayProof
        }
        
        [<Literal>]
        let Collection =
            "Items"
        
        let getCollection (db : IMongoDatabase) : IMongoCollection<T> =
            db.GetCollection<T>( Collection )
        
        let drop (db : IMongoDatabase) : unit =
            db.DropCollection( Collection )
        
        let createIndex (db : IMongoDatabase) : string =
            [| Builders.IndexKeys.Descending (FieldDefinition<T>.op_Implicit "timestamp")
            ;  Builders.IndexKeys.Ascending (FieldDefinition<T>.op_Implicit "name")
            |]
            |> Builders.IndexKeys.Combine
            |> CreateIndexModel
            |> (getCollection db).Indexes.CreateOne
        
        let put (db : IMongoDatabase) (value : T) : unit =
            (getCollection db).InsertOne value
            createIndex db |> ignore
        
        let puts (db : IMongoDatabase) (values : T seq) : unit =
            (getCollection db).InsertMany values
            createIndex db |> ignore
        
        let get (db : IMongoDatabase) (root : Hash) (name : string) : T =
            (getCollection db).Find( fun t -> t.name = name && t.root = root ).Single()
        
        let tryGet (db : IMongoDatabase) (root : Hash) (name : string) : T option =
            try
                get db root name
                |> Some
            with _ ->
                None
        
        let getByTimeBounds (db : IMongoDatabase) (skip : Nullable<int>) (take : Nullable<int>) (low : uint64) (high : uint64) : T seq =
            (getCollection db).Find( fun t -> low <= t.timestamp && t.timestamp <= high ).Skip(skip).Limit(take).ToEnumerable()
        
        let getByTimeBoundsAndName (db : IMongoDatabase) (skip : Nullable<int>) (take : Nullable<int>) (low : uint64) (high : uint64) (name : string) : T seq =
            (getCollection db).Find( fun t -> low <= t.timestamp && t.timestamp <= high && t.name = name ).Skip(skip).Limit(take).ToEnumerable()
        
        let getItem (db : IMongoDatabase) (root : Hash) ((name , item) : string * Item) : T =
            (getCollection db).Find( fun t -> t.name = name && t.root = root && t.item = item ).Single()
        
        let getItems (db : IMongoDatabase) (root : Hash) (items : Record) : T list =
            let mapper (name , item) =
                (getCollection db).Find( fun t -> t.name = name && t.root = root && t.item = item ).ToEnumerable()
                |> Seq.toList
            
            items
            |> Map.toList
            |> List.map mapper
            |> List.concat
        
        let count  (db : IMongoDatabase) : int64 =
            (getCollection db).CountDocuments(Builders<T>.Filter.Empty)
        
        let countByTimeBounds  (db : IMongoDatabase) (low : uint64) (high : uint64) : int64 =
            (getCollection db).CountDocuments( fun t -> low <= t.timestamp && t.timestamp <= high )
        
        let countByTimeBoundsAndName  (db : IMongoDatabase) (low : uint64) (high : uint64) (name : string) : int64 =
            (getCollection db).CountDocuments( fun t -> low <= t.timestamp && t.timestamp <= high && t.name = name )
    
    
    module Commit =
        
        type T = {
            _id       : BsonObjectId
            commitId  : Hash
            timestamp : timestamp
            root      : Hash
            items     : string[]
        }
        
        [<Literal>]
        let Collection =
            "Commits"
        
        [<Literal>]
        let Index =
            "commitId"
        
        let getCollection (db : IMongoDatabase) : IMongoCollection<T> =
            db.GetCollection<T>( Collection )
        
        let drop (db : IMongoDatabase) : unit =
            db.DropCollection( Collection )
        
        let createIndex (db : IMongoDatabase) : string =
            (FieldDefinition<T>.op_Implicit Index)
            |> Builders.IndexKeys.Ascending
            |> CreateIndexModel
            |> (getCollection db).Indexes.CreateOne

        let put (db : IMongoDatabase) (value : T) : unit =
            (getCollection db).InsertOne value
            createIndex db |> ignore
        
        let get (db : IMongoDatabase) (commitId : Hash) : T =
            (getCollection db).Find( fun t -> t.commitId = commitId ).Single()
        
        let tryGet (db : IMongoDatabase) (commitId : Hash) : T option =
            try
                get db commitId
                |> Some
            with ex ->
                None
        
        let count  (db : IMongoDatabase) : int64 =
            (getCollection db).CountDocuments(Builders<T>.Filter.Empty)
    
    
    module TimeRootCommit =
        
        type T = {
           _id       : BsonObjectId
           timestamp : timestamp
           root      : Hash
           commitId  : Hash
        }
        
        [<Literal>]
        let Collection =
            "TimeCommits"
        
        [<Literal>]
        let Index =
            "timestamp"
        
        let getCollection (db : IMongoDatabase) : IMongoCollection<T> =
            db.GetCollection<T>( Collection )
        
        let drop (db : IMongoDatabase) : unit =
            db.DropCollection( Collection )
        
        let createIndex (db : IMongoDatabase) : string =
            (FieldDefinition<T>.op_Implicit Index)
            |> Builders.IndexKeys.Ascending
            |> CreateIndexModel
            |> (getCollection db).Indexes.CreateOne
        
        let put (db : IMongoDatabase) (value : T) : unit =
            (getCollection db).InsertOne value
            createIndex db |> ignore
        
        let get (db : IMongoDatabase) (timestamp : timestamp) (root : Hash) : T =
            (getCollection db).Find( fun t -> t.timestamp = timestamp && t.root = root ).Single()
        
        let tryGet (db : IMongoDatabase) (timestamp : timestamp) (root : Hash) : T option =
            try
                get db timestamp root
                |> Some
            with _ ->
                None
        
        let count  (db : IMongoDatabase) : int64 =
            (getCollection db).CountDocuments(Builders<T>.Filter.Empty)
        
        module ByTimestamp =
            
            let get (db : IMongoDatabase) (timestamp : timestamp) : T seq =
                (getCollection db).Find( fun t -> t.timestamp = timestamp ).ToEnumerable()
            
            let search (db : IMongoDatabase) (low : timestamp) (high : timestamp) : T seq =
                (getCollection db).Find( fun t -> low <= t.timestamp && t.timestamp <= high ).ToEnumerable()
        
        module ByRoot =
            
            let get (db : IMongoDatabase) (root : Hash) : T seq =
                (getCollection db).Find( fun t -> t.root = root ).ToEnumerable()



module Commit =
    
    type T = {
        commitId  : Hash
        timestamp : timestamp
        root      : Hash
        items     : Map<string, Item * Merkle.Proof>
    }
    
    let put (db : IMongoDatabase) (t : T) : unit =
        
        let createItem (name : string, (item : Item, proof : Merkle.Proof)) : Collections.Item.T =
            {
                _id       = MongoDB.Bson.BsonObjectId(MongoDB.Bson.ObjectId.GenerateNewId())
                name      = name
                root      = t.root
                commitId  = t.commitId
                timestamp = t.timestamp
                item      = item
                proof     = { index = proof.index ; path = proof.path |> List.toArray }
            }
        
        let commitIsNew =
            (Collections.Commit.getCollection db).Find( fun d -> d.commitId = t.commitId ).ToEnumerable()
            |> Seq.isEmpty
        
        let rootIsNew =
            (Collections.Commit.getCollection db).Find( fun d -> d.root = t.root ).ToEnumerable()
            |> Seq.isEmpty
        
        if commitIsNew then
            Collections.Commit.put db
                {
                    _id       = MongoDB.Bson.BsonObjectId(MongoDB.Bson.ObjectId.GenerateNewId())
                    commitId  = t.commitId
                    timestamp = t.timestamp
                    root      = t.root
                    items     = t.items |> Map.toArray |> Array.map fst
                }
            
            if rootIsNew then 
                t.items
                |> Map.toSeq
                |> Seq.map createItem
                |> Collections.Item.puts db
            
            Collections.TimeRootCommit.put db
                {
                    _id       = MongoDB.Bson.BsonObjectId(MongoDB.Bson.ObjectId.GenerateNewId())
                    timestamp = t.timestamp
                    root      = t.root
                    commitId  = t.commitId
                }



let dropAll (db : IMongoDatabase) : unit =
    Collections.PublicKey      .drop db
    Collections.Commit         .drop db
    Collections.TimeRootCommit .drop db
    Collections.Item           .drop db
