module WalletActions

open FSharp.Data
open FSharpx.Reader

open Api.Types

open Config
open Request

type Program<'a> = ProgramMonad.Program<'a>



module Execute =
    
    [<Literal>]
    let private endpoint =
        "wallet/contract/execute"

    [<Literal>]  
    let private error =
        "Error executing contract"

    type Env = {
        address       : string
        command       : string
        messageBody   : string
        returnAddress : bool
    }
    
    let getJson (env : Env) : Configured<JsonValue> = fun config ->
        ( new ContractExecuteRequestJson.Root
            ( env.address
            , env.command
            , env.messageBody
            , new ContractExecuteRequestJson.Options
                ( env.returnAddress
                , config.envVars.derivationPath
                )
            , Array.empty
            , config.envVars.walletPass
            )
        ).JsonValue
    
    let act (env : Env) : Configured<HttpResponse> =
        getJson env
        >>= Response.action endpoint
    
    let handle (env : Env) : Program<string> =
        Response.getBody error
        >> Result.mapError Error.API
        <!> act env

module getPublicKey =
    
    [<Literal>]
    let private endpoint =
        "wallet/publickey"
    
    [<Literal>]  
    let private error =
        "Error requesting the publicKey"
    
    let getJson : Configured<JsonValue> = fun config ->
        ( new GetPublicKeyJson.Root
            ( config.envVars.derivationPath
            , config.envVars.walletPass
            )
        ).JsonValue
    
    let act : Configured<HttpResponse> =
        getJson
        >>= Response.action endpoint
    
    let handle : Program<string> =
        Response.getBody error
        >> Result.mapError Error.API
        <!> act
