module Item



type INumeral =
    | IByte   of byte
    | ISByte  of sbyte
    | IInt16  of int16
    | IUInt16 of uint16
    | IInt32  of int32
    | IUInt32 of uint32
    | IInt64  of int64
    | IUInt64 of uint64
    | IDec    of decimal
    | ISingle of single
    | IDouble of double

type Item =
    | INull
    | IBool of bool
    | IRec  of (string * Item) []
    | INum  of INumeral
    | IStr  of string
    | IArr  of Item []
