module Merkle

open Consensus



type Hash = Hash.Hash



type Proof = {
    index : int
    path  : Hash list
}



type Merkle =
    | Leaf of Hash
    | Node of Hash * Merkle * Merkle
    with
    
    member this.hash : Hash =
        match this with
        | Leaf(h) ->
            h
        | Node(h, _, _) ->
            h
    
    /// Tree catamorphism (structural recursion) - recurse over the tree structure (from leaves to root)
    member this.fold
        ( x : Hash -> 'a             )
        ( f : Hash -> 'a -> 'a -> 'a )
        : 'a =
        match this with
        | Leaf(h) ->
            x h
        | Node(h, l, r) ->
            f h (l.fold x f) (r.fold x f)
    
    /// Tree anamorphism (structural corecursion) - generate a tree (from root to leaves)
    static member generate
        ( f    : 'a -> Result< Hash , Hash * 'a * 'a > )
        ( seed : 'a                                    )
        : Merkle =
        
        let rec gen x = 
            match f x with
            | Ok(h) ->
                Leaf(h)
            | Error(h, l, r) ->
                Node(h, gen l, gen r)
        
        gen seed
    
    /// The number of leaves in the tree
    member this.size : int =
        this.fold (fun _ -> 1) (fun _ l r -> l + r)
    
    /// The height of the tree
    member this.height : int =
        this.fold (fun _ -> 0) (fun _ l r -> max l r + 1)



let private log2 x =
    log x / log 2.0

/// 2 ^ ( ceil(log2(x)) - 1)  
let findSplitIndex length =
    if length = 1 then
        1
    else
        length
        |> float
        |> log2
        |> ceil
        |> fun x -> x - 1.0
        |> int
        |> fun x -> 1 <<< x 

let private innerPrefix =
    [|105uy|] // 'i'

let rec computeMerkleTree (dataset : Hash array) : Merkle =
    
    let rec aux (low : int) (high : int) : Merkle =
        if low + 1 = high then
            let hash = Array.get dataset low
            Leaf(hash)
        else
            let splitIndex = low + findSplitIndex (high - low)
            let left = aux low splitIndex
            let right = aux splitIndex high
            let root = Hash.computeMultiple (seq {yield innerPrefix;  yield (Hash.bytes left.hash); yield (Hash.bytes right.hash)})
            Node(root, left, right)
    
    aux 0 dataset.Length



module AuditPath =
    
    let generate (tree : Merkle) : Hash list [] =
        let paths = Array.create tree.size []
        
        let rec aux t low high path : unit =
            match t with
            | Leaf(_) ->
                paths.[low] <- path
            | Node(_, l, r) ->
                let splitIndex = low + findSplitIndex (high - low)
                aux l low (splitIndex - 1) (r.hash :: path)
                aux r splitIndex high (l.hash :: path)
                
        aux tree 0 tree.size []
        
        paths
        |> Array.map List.rev
    
    let createAuditPath (tree : Merkle) (index : int) : Hash list =
        
        let rec aux t low high path : Hash list =
            match t with
            | Leaf(_) ->
                path
            | Node(_, l, r) ->
                let splitIndex = low + findSplitIndex (high - low)
                if index < splitIndex then
                    aux l low (splitIndex - 1) (r.hash :: path)
                else
                    aux r splitIndex high (l.hash :: path)

        aux tree 0 tree.size []
        |> List.rev
    
    /// 2 ** n    (2 to the power of n)
    let inline private exp2 (n : int) : int = 1 <<< n
    
    /// n / 2    (divide n by 2)
    let inline private div2 (n : int) : int = n >>> 1
    
    let inline private orShift n t = t ||| (t >>> n)
    
    /// The highest power of 2 which is smaller or equal to the input
    let inline private floor2 x =
        // (using a bitwise hack for optimization)
        x
        |> orShift 1
        |> orShift 2
        |> orShift 4
        |> orShift 8
        |> orShift 16
        |> fun x -> (x + 1) >>> 1
    
    let indexToLocation (height : int) (index : int) : int =
        
        let rec aux exp2height index acc =
            if index < max 1 (div2 exp2height) then // check if the leaf is in the left subtree
                // for left subtree - compute directly
                acc + exp2height + index
            else
                // for right subtree - recurse
                aux (div2 exp2height) (index - floor2 index) (acc + exp2height)
        
        aux (exp2 height) index 0
    
    let rec private reconstructRootWithLocation (hash : Hash) (location : int) (revPath : Hash list) : Hash =
        match revPath with
        | [] ->
            hash
        | hd :: tl ->
            let root = 
                if location &&& 1 = 0 then
                    Hash.computeMultiple (seq {yield innerPrefix; yield Hash.bytes hash; yield Hash.bytes hd})
                else
                    Hash.computeMultiple (seq {yield innerPrefix; yield Hash.bytes hd; yield Hash.bytes hash})
            reconstructRootWithLocation root (location >>> 1) tl
    
    let reconstructRoot (path : Hash list) (index : int) (hash : Hash) : Hash =
        let loc = if List.isEmpty path then 0 else (indexToLocation (List.length path) index)
        reconstructRootWithLocation hash loc (List.rev path)
    
    let verify (root : Hash) (path : Hash list) (index : int) (hash : Hash) =
        reconstructRoot path index hash = root
