module Primitives

open System

open Org.BouncyCastle.Crypto.Digests

open Consensus.Hash



type Sha3Update<'a> = 'a -> Sha3Digest -> Sha3Digest



let private ensureBigEndian : byte[] -> byte[] =
    if BitConverter.IsLittleEndian then
        Array.rev
    else
        id

/// Update the message digest with a block of bytes.
///     length - length of the data
///     input  - byte array containing the data
///     sha3   - digest to update
let private blockUpdate
    ( length : int        )
    ( input  : byte[]     )
    ( sha3   : Sha3Digest )
    : Sha3Digest
    =
        let digest = Sha3Digest sha3
        digest.BlockUpdate(input, 0, length)
        digest



module byte =
    
    type T = byte
    
    let serialize : T -> byte[] =
        Array.singleton
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 1



module sbyte =
    
    type T = sbyte
    
    let serialize : T -> byte[] =
        fun _ -> failwith "sbyte.serialize not implemented yet"
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 1



module int16 =
    
    type T = int16
    
    let serialize : T -> byte[] =
        BitConverter.GetBytes >> ensureBigEndian
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 2



module uint16 =
    
    type T = uint16
    
    let serialize : T -> byte[] =
        BitConverter.GetBytes >> ensureBigEndian
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 2



module int32 =
    
    type T = int32
    
    let serialize : T -> byte[] =
        BitConverter.GetBytes >> ensureBigEndian
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 4



module uint32 =
    
    type T = uint32
    
    let serialize : T -> byte[] =
        BitConverter.GetBytes >> ensureBigEndian
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 4



module int64 =
    
    type T = int64
    
    let serialize : T -> byte[] =
        BitConverter.GetBytes >> ensureBigEndian
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 8



module uint64 =
    
    type T = uint64
    
    let serialize : T -> byte[] =
        BitConverter.GetBytes >> ensureBigEndian
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 8



module decimal =
    
    type T = decimal
    
    let serialize : T -> byte[] =
        Decimal.GetBits >> Array.map BitConverter.GetBytes >> Array.concat
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 16



module single =
    
    type T = single
    
    let serialize : T -> byte[] =
        BitConverter.GetBytes >> ensureBigEndian
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 4



module double =
    
    type T = double
    
    let serialize : T -> byte[] =
        BitConverter.GetBytes >> ensureBigEndian
    
    
    module Sha3 =
        
        let update : Sha3Update<T> = 
            serialize >> blockUpdate 8



module string =
    
    type T = string
    
    let serialize : T -> byte[] =
        Text.Encoding.ASCII.GetBytes
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            fun s ->
                let ss = serialize s
                blockUpdate (Array.length ss) ss



module bool =
    
    type T = bool
    
    let serialize : T -> byte[] =
        BitConverter.GetBytes >> ensureBigEndian
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 1



module Hash =
    
    type T = Hash
    
    let serialize : T -> byte[] =
        Consensus.Hash.bytes
    
    
    module Sha3 =
        
        let update : Sha3Update<T> =
            serialize >> blockUpdate 32
