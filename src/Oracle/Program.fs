
open System.IO

open Argu
open Infrastructure

open Logary.Configuration
open Logary.Targets
open Logary.Message

open Config
open Error
open ProgramMonad
open Interface
open Html

module Choice = FSharpx.Choice



Serialize.registerAll()



let display =
    function
    | Ok res ->
        printfn "%s" res
        0
    | Error (Error.Argument Error.NoArguments) ->
        0
    | Error err ->
        eprintfn "%s" (Error.parseError err)
        1

let displayUnit =
    function
    | Ok() ->
        0
    | Error err ->
        eprintfn "%A" err
        1

let checkTxField createTx fieldRes : Program<string>  =
    program {
        if createTx then
            return fieldRes
        else
            return! ofResult (Error <| Error.Argument InvalidTxWithNoTx)
    }



module Usage =
    
    type ArgumentsHandler<'cmd,'res> when 'cmd :> IArgParserTemplate
        = ArgumentParser<'cmd> -> ParseResults<'cmd> -> Program<'res>

    let empty (handler : ArgumentsHandler<'cmd,'res>) (parser : ArgumentParser<'cmd>) (args : ParseResults<'cmd>) : Program<'res> =
        if args.GetAllResults() |> List.isEmpty || args.IsUsageRequested then
            parser.PrintUsage() |> printfn "%s";
            error <| Error.Argument Error.NoArguments
        else
            handler parser args

    let helpOnly (handler : ArgumentsHandler<'cmd,'res>) (parser : ArgumentParser<'cmd>) (args : ParseResults<'cmd>) : Program<'res> =
        if args.IsUsageRequested then
            parser.PrintUsage() |> printfn "%s";
            error <| Error.Argument Error.NoArguments
        else
            handler parser args



module Commit =
    
    type Cmd =
        | [<MainCommand; Unique>]
            File of filename : string
        | [<Unique ; AltCommandLine("-t")>]
            Timestamp of timestamp : uint64
        | [<Unique ; AltCommandLine("-x")>]
            NoTx
        | [<Unique ; AltCommandLine("-i")>]
            Stdin
        | [<Unique ; AltCommandLine("-f")>]
            Field of field : string
    with
        interface IArgParserTemplate with
            member arg.Usage =
                match arg with
                | File      _ -> "file name of the committed data JSON file."
                | Timestamp _ -> "time of the committed data (in milliseconds since the Unix epoch)."
                | NoTx        -> "don't create a transaction, just return the message body."
                | Stdin       -> "when this flag is used - get JSON string from standard input."
                | Field     _ -> "output only the specified field."
    
    let handle (parser : ArgumentParser<Cmd>) (args : ParseResults<Cmd>) : Program<string> =
        program {
            let! json =
                match args.TryGetResult Stdin with
                | None ->
                    args.TryGetResult File
                    |> withError (Error.Argument <| Error.ArgumentNotFound (parser.GetArgumentCaseInfo File).Name.Value)
                    >>= (File.ReadAllText >> ret)
                | Some _ ->
                    match args.TryGetResult File with
                    | None ->
                        System.Console.In.ReadLine()
                        |> ret
                    | Some _ ->
                        Error.Argument Error.InvalidArgumentsCombination
                        |> error
            
            let timestamp =
                args.TryGetResult Timestamp
                |> Option.defaultWith (Timestamp.now)

            let createTx =
                args.TryGetResult NoTx
                |> Option.isNone
            
            let! result = Interface.Commit.handle json timestamp createTx
            
            match args.TryGetResult Field with
            | Some field ->
                return!
                    getField field result
                    >>= checkTxField createTx
            | None ->
                return
                    string result.toJson
        }



module Query =
    
    module TimeBounds =
        
        type Cmd =
            | [<Unique ; AltCommandLine("-l")>]
                Low of low : uint64
            | [<Unique ; AltCommandLine("-h")>]
                High of high : uint64
            | [<Unique ; AltCommandLine("-k")>]
                Key of key : string
            | [<Unique ; AltCommandLine("-s")>]
                Skip of n : int
            | [<Unique ; AltCommandLine("-t")>]
                Take of n : int
            | [<Unique ; CliPrefix(CliPrefix.None) ; AltCommandLine("c")>]
                Count
        with
            interface IArgParserTemplate with
                member arg.Usage =
                    match arg with
                    | Low     _ -> "lower time bound."
                    | High    _ -> "upper time bound."
                    | Key     _ -> "key to search the value for."
                    | Skip    _ -> "skip the first <n> items."
                    | Take    _ -> "take only the first <n> items (after the skip if there is one)."
                    | Count   _ -> "return the total amount of items satisfying the query."
        
        let handle (_ : ArgumentParser<Cmd>) (args : ParseResults<Cmd>) : Program<Choice<Query.TimeBounds.Return list , int64>> =
            program {
                let! config =
                    ask
                
                let low =
                    args.TryGetResult Low

                let high =
                    args.TryGetResult High

                let key =
                    args.TryGetResult Key
                
                let skip =
                    args.TryGetResult Skip
                
                let take =
                    args.TryGetResult Take
                
                let count =
                    args.TryGetResult Count
                
                let! db =
                    DataAccess.connectToDatabase config None
                    |> ofOption Error.DatabaseDisconnected
                
                match count with
                | None ->
                    return!
                        Interface.Query.TimeBounds.handle db skip take low high key
                        |> Choice<Query.TimeBounds.Return list , int64>.Choice1Of2
                        |> Ok
                        |> ofResult
                | Some _ ->
                    return!
                        Interface.Query.TimeBoundsCount.handle db low high key
                        |> Choice<Query.TimeBounds.Return list , int64>.Choice2Of2
                        |> Ok
                        |> ofResult
            }
    
    module TimeKeyValue =
        
        type Cmd =
            | [<MainCommand; Unique>]
                File of filename : string
            | [<Unique ; AltCommandLine("-i")>]
                Stdin
        with
            interface IArgParserTemplate with
                member arg.Usage =
                    match arg with
                    | File  _ -> "file name of the query data JSON file."
                    | Stdin   -> "when this flag is used - get JSON string from standard input."
        
        let handle (parser : ArgumentParser<Cmd>) (args : ParseResults<Cmd>) : Program<Interface.Query.TimeKeyValue.Return list> =
            program {
                let! config =
                    ask
                
                let! json =
                    match args.TryGetResult Stdin with
                    | None ->
                        args.TryGetResult File
                        |> withError (Error.Argument <| Error.ArgumentNotFound (parser.GetArgumentCaseInfo File).Name.Value)
                        >>= (File.ReadAllText >> ret)
                    | Some _ ->
                        match args.TryGetResult File with
                        | None ->
                            System.Console.In.ReadLine()
                            |> ret
                        | Some _ ->
                            Error.Argument Error.InvalidArgumentsCombination
                            |> error

                let items =
                    Interface.Query.TimeKeyValue.parseJson json
                
                let! db =
                    DataAccess.connectToDatabase config None
                    |> ofOption Error.DatabaseDisconnected
                
                return!
                    Interface.Query.TimeKeyValue.handle db items
            }
    
    type Cmd =
        | [<Unique ; CliPrefix(CliPrefix.None) ; AltCommandLine("t")>]
            TimeBounds of ParseResults<TimeBounds.Cmd>
        | [<Unique ; CliPrefix(CliPrefix.None) ; AltCommandLine("k")>]
            TimeKeyValue of ParseResults<TimeKeyValue.Cmd>
        | [<Unique ; CliPrefix(CliPrefix.None) ; AltCommandLine("p")>]
            PublicKey
    with
        interface IArgParserTemplate with
            member arg.Usage =
                match arg with
                | TimeBounds _   -> "search for the values of a specific key within specific time bounds in the committed data."
                | PublicKey      -> "get the public key of the provider."
                | TimeKeyValue _ -> "given a list of names and time frames (as a JSON string) return the values of those names within the given time frames."
    
    let handle (parser : ArgumentParser<Cmd>) (cmds : ParseResults<Cmd>) : Program<string> =
        match cmds.GetAllResults() with
        | TimeBounds args :: [] ->
            Usage.empty TimeBounds.handle (parser.GetSubCommandParser TimeBounds) args
            |@> Choice.choice (Query.QTimeBounds >> fun res -> string res.toJson) (Query.QTimeBoundsCount >> fun res -> string res.toJson)
        | PublicKey :: [] ->
            Interface.Query.PublicKey.handle
            |@> Query.QPublicKey
            |@> fun res -> string res.toJson
        | TimeKeyValue args :: [] ->
            Usage.empty TimeKeyValue.handle (parser.GetSubCommandParser TimeKeyValue) args
            |@> Query.QTimeKeyValue
            |@> fun res -> string res.toJson
        | _ ->
            Error.Argument Error.InvalidArgumentsCombination
            |> error



module Attest =
    
    type Cmd =
        | [<Unique ; AltCommandLine("-r")>]
            Root of root : string
        | [<Unique ; AltCommandLine("-t")>]
            Timestamp of timestamp : uint64
        | [<Unique ; AltCommandLine("-c")>]
            Commit of commit : string
        | [<Unique ; AltCommandLine("-p")>]
            PublicKey of pk : string
        | [<Unique ; AltCommandLine("-d")>]
            Contract of cid : string
        | [<Unique ; AltCommandLine("-x")>]
            NoTx
        | [<Unique ; AltCommandLine("-f")>]
            Field of field : string
    with
        interface IArgParserTemplate with
            member arg.Usage =
                match arg with
                | Root      _ -> "root of the committed Merkle tree."
                | Timestamp _ -> "time of the committed data (in milliseconds since the Unix epoch)."
                | Commit    _ -> "commit ID of the committed data."
                | PublicKey _ -> "public key of the recipient address."
                | Contract  _ -> "contract ID of the recipient contract."
                | NoTx        -> "don't create a transaction, just return the message body."
                | Field     _ -> "output only the specified field."
    
    let handle (_ : ArgumentParser<Cmd>) (args : ParseResults<Cmd>) : Program<string> =
        program {
            let! config =
                ask
            
            let createTx =
                args.TryGetResult NoTx
                |> Option.isNone
            
            let recip =
                Option.map Interface.Attest.PK (args.TryGetResult PublicKey)
                |> Option.orElse (Option.map Interface.Attest.CID (args.TryGetResult Contract))
            
            let! db =
                DataAccess.connectToDatabase config None
                |> ofOption Error.DatabaseDisconnected
            
            match args.TryGetResult Commit with
            | Some commit ->
                return!
                    Interface.Attest.ByCommit.handle db commit recip createTx
                    >>= fun result ->
                    match args.TryGetResult Field with
                    | Some field ->
                        getField field result
                        >>= checkTxField createTx
                    | None ->
                        ret <| string result.toJson
            | None ->
                return! FSharpx.Option.maybe {
                    let! root      = args.TryGetResult Root
                    let! timestamp = args.TryGetResult Timestamp
                    return Interface.Attest.ByRootAndTimestamp.handle db root timestamp recip createTx
                }
                |> Option.defaultWith (fun () -> error <| Error.Argument Error.NoArguments)
                >>= fun result ->
                    match args.TryGetResult Field with
                    | Some field ->
                        getField field result
                        >>= checkTxField createTx
                    | None ->
                        ret <| string result.toJson
        }


module Audit =
    
    type Cmd =
        | [<MainCommand; Unique>]
            Item of item : string
        | [<Unique ; AltCommandLine("-c")>]
            Commit of commit : string
        | [<Unique ; AltCommandLine("-r")>]
            Root of root : string
        | [<Unique ; AltCommandLine("-i")>]
            Stdin
    with
        interface IArgParserTemplate with
            member arg.Usage =
                match arg with
                | Commit    _ -> "commit hash."
                | Root      _ -> "root hash."
                | Item      _ -> "return the audit path of the given item."
                | Stdin       -> "when this flag is used - get item's JSON string from standard input (can't be used along with <item>)."
    
    let handle (parser : ArgumentParser<Cmd>) (args : ParseResults<Cmd>) : Program<string> =
        program {
            let! config =
                ask
            
            let! json =
                begin match args.TryGetResult Stdin with
                | None ->
                    args.TryGetResult Item
                    |> withError (Error.Argument <| Error.ArgumentNotFound (parser.GetArgumentCaseInfo Item).Name.Value)
                    >>= (File.ReadAllText >> ret)
                | Some _ ->
                    match args.TryGetResult Item with
                    | None ->
                        System.Console.In.ReadLine()
                        |> ret
                    | Some _ ->
                        Error.Argument Error.InvalidArgumentsCombination
                        |> error
                end
                >>= JSON.parse
            
            let commitOpt() =
                args.TryGetResult Commit
                |> Option.map Choice1Of2
            
            let rootOpt() =
                args.TryGetResult Root
                |> Option.map Choice2Of2
            
            let! commitOrRoot =
                commitOpt()
                |> Option.orElseWith rootOpt
                |> withError (Error.Argument <| Error.ArgumentNotFound (parser.GetArgumentCaseInfo Commit).Name.Value)
            
            let! db =
                DataAccess.connectToDatabase config None
                |> ofOption Error.DatabaseDisconnected
            
            match commitOrRoot with
            | Choice1Of2 commit ->
                return!
                    Interface.Audit.ByCommit.handle db commit json
                    |@> fun res -> string res.toJson
            | Choice2Of2 root ->
                return!
                    Interface.Audit.ByRoot.handle db root json
                    |@> fun res -> string res.toJson
        }



module Server =
    
    type Cmd =
        | [<AltCommandLine("-b")>]
            Bind of address : string
        | [<AltCommandLine("-c")>]
            Chain of chain : string
        | [<AltCommandLine("-o")>]
            Origin of origin : string
        | [<AltCommandLine("-l")>]
            MaxTake of n : uint64
        | [<AltCommandLine("-s")>]
            MaxBodySize of n : uint64
    with
        interface IArgParserTemplate with
            member arg.Usage =
                match arg with
                | Bind   _ -> "API port."
                | Chain  _ -> "node chain."
                | Origin _ -> "CORS origin."
                | MaxTake _ -> "maximum size of take (default: 1000)."
                | MaxBodySize _ -> "maximum size of body for the getValues endpoint (default: 1000)."
    
    let handle (_ : ArgumentParser<Cmd>) (args : ParseResults<Cmd>) =
        program {
            let! config =
                ask
            
            let chain =
                FSharpx.Option.maybe {
                     match! args.TryGetResult Chain with
                     | "main" -> return Consensus.Chain.Main
                     | "test" -> return Consensus.Chain.Test
                     | _      -> return Consensus.Chain.Local
                }
                |> Option.defaultValue (Consensus.Chain.Local) //TODO: Change to Main
            
            let origin =
                match args.TryGetResult Origin with
                | Some any when any = "any" ->
                    Http.Any
                | Some custom ->
                    Http.Custom custom
                | None ->
                    Http.No
            
            let bind =
                args.TryGetResult Bind
            
            let maxTake =
                args.TryGetResult MaxTake
            
            let maxBody =
                args.TryGetResult MaxBodySize
            
            let config =
                { config with
                    envVars =
                        bind
                        |> Option.map (fun address -> {config.envVars with oracleApi = address})
                        |> Option.defaultValue config.envVars
                    chain =
                        chain
                    origin =
                        origin
                    querySizeLimit =
                        maxTake
                        |> Option.defaultValue config.querySizeLimit
                    bodySizeLimit =
                        maxBody
                        |> Option.defaultValue config.bodySizeLimit
                }
            
            eventX "Actor running... press CTRL+C to exit"
            |> Log.info
            
            Html.InitActor.create config |> ignore
            
            return sprintf "Running server on port: %A" (Infrastructure.Endpoint.getPort config.envVars.oracleApi)
        }


module Fields =
    
    let fields : (string * string) list =
        [ "root"               , "root hash of the committed Merkle tree."
        ; "timestamp"          , "time stamp (in UNIX Epoch format) of the commitment."
        ; "commit"             , "commit ID (derived from the root, the timestamp, and the public key of the data provider)."
        ; "tx"                 , "transaction hash of the generated transaction (doesn't work with --notx)."
        ; "messageBody"        , "message body generated for the oracle contract (JSON format)."
        ; "messageBodyEncoded" , "message body generated for the oracle contract (hexadecimal format)."
        ]
    
    let handle() : Program<string> =
        
        let maxlen =
            fields
            |> List.map (fst >> String.length)
            |> List.max
        
        fields
        |> List.map (fun (field,desc) -> sprintf "%-*s    %-5s" maxlen field desc)
        |> String.concat "\n"
        |> ret


module Main =
    
    type Cmd =
        | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("c")>]
            Commit of ParseResults<Commit.Cmd>
        | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("q")>]
            Query of ParseResults<Query.Cmd>
        #if DEBUG
        | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("r")>]
            Clear
        #endif
        | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("a")>]
            Attest of ParseResults<Attest.Cmd>
        | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("p")>]
            Audit of ParseResults<Audit.Cmd>
        | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("s")>]
            Server of ParseResults<Server.Cmd>
        | [<CliPrefix(CliPrefix.None)>]
            Fields
    with
        interface IArgParserTemplate with
            member s.Usage =
                match s with
                | Commit _ -> "commit an item or a data set."
                | Query  _ -> "query for committed data."
                #if DEBUG
                | Clear  _ -> "clear and reset the database."
                #endif
                | Attest _ -> "attest on committed data."
                | Audit  _ -> "get the audit path for a committed item."
                | Server _ -> "create a web server."
                | Fields   -> "list output fields."
    
    let parser =
        ArgumentParser.Create<Cmd>("zen-oracle")
    
    module Parser =
        let Commit = parser.GetSubCommandParser Commit
        let Query  = parser.GetSubCommandParser Query
        let Attest = parser.GetSubCommandParser Attest
        let Audit  = parser.GetSubCommandParser Audit
        let Server = parser.GetSubCommandParser Server
    
    let handleCommand (cmd : Cmd) : Configured<int> =
        match cmd with
        | Commit args -> display << Usage.empty    Commit .handle Parser.Commit args
        | Query  args -> display << Usage.empty    Query  .handle Parser.Query  args
        | Attest args -> display << Usage.empty    Attest .handle Parser.Attest args
        | Audit  args -> display << Usage.empty    Audit  .handle Parser.Audit  args
        | Server args -> display << Usage.helpOnly Server .handle Parser.Server args
        | Fields      -> display <<                Fields .handle()
        #if DEBUG
        | Clear       -> displayUnit << Interface.Clear.handle
        #endif
    
    let handle (cmds : ParseResults<Cmd>) : Configured<int> =
        match cmds.GetAllResults() with
        | [] ->
            fun _ ->
                printfn "%s" <| parser.PrintUsage()
                0
        | cmd :: _ ->
            handleCommand cmd
    
    let run (argv : string[]) : Configured<int> =
        parser.ParseCommandLine(inputs = argv, raiseOnUsage = false)
        |> handle



[<EntryPoint>]
let main argv =
    
    try
        
        let config = Config.defaultConfig
        
        Main.run argv config
    
    with ex ->
        
        eventX (sprintf "%s" ex.Message)
        |> Log.error
        
        eprintfn "%A" ex
        
        1
