module Record

open Item



type Record = Map<string, Item>



let sort<'a> : Map<string, 'a> -> (string * 'a) [] =
    Map.toArray >> Array.sortBy fst
