module ProgramMonad

open FSharpx.Prelude

open Error
open Config

module List = FSharpx.Collections.List



type Reader<'env, 'a> = FSharpx.Reader.Reader<'env, 'a>

/// Specialized ReaderResult monad transformer
type Program<'a> = Reader<Config, Result<'a, Error>>



let ret (x : 'a) : Program<'a> =
    fun _ -> Ok x

let bind (k : 'a -> Program<'b>) (m : Program<'a>) : Program<'b> =
    fun conf -> (Result.bind (flip k conf)) (m conf)



type ProgramBuilder() =
    
    member this.Return (x : 'a) : Program<'a> =
        ret x
    
    member this.ReturnFrom (x : Program<'a>) : Program<'a> =
        x
    
    member this.Bind (m : Program<'a> , k : 'a -> Program<'b>) : Program<'b> =
        bind k m
    
    member this.Zero() : Program<unit> =
        this.Return ()
    
    member this.Combine(x : Program<unit>, y : Program<'a>) : Program<'a> =
        this.Bind(x, fun () -> y)
    
    member this.TryWith(m : Program<'a> , h : exn -> Program<'a>) : Program<'a> =
        fun conf -> try m conf with e -> (h e) conf
    
    member this.TryFinally(m : Program<'a>, finale : unit -> unit) : Program<'a> =
        fun conf -> try m conf finally finale()
    
    member this.Using(dis : #System.IDisposable , k : 'a -> Program<'b>) : Program<'b> =
        this.TryFinally(k dis , fun () -> if not (isNull (box dis)) then dis.Dispose())
    
    member this.Delay(f : unit -> Program<'a>) : Program<'a> =
        this.Bind(this.Return (), f)
    
    member this.While(guard : unit -> bool, m : Program<unit>) : Program<unit> =
        if not(guard()) then this.Zero() else this.Bind(m, (fun () -> this.While(guard, m)))
    
    member this.For(s : seq<'a>, k : 'a -> Program<unit>) : Program<unit> =
        this.Using(s.GetEnumerator(), (fun enum -> this.While(enum.MoveNext, this.Delay(fun () -> k enum.Current))))



let program = ProgramBuilder()

let ask : Program<Config> =
    Ok

let asks (f : Config -> 'a) : Program<'a> =
    bind (ret << f) ask

let local (f : 'a2 -> 'a1) (m : Program<'a2>) : Program<'a1> =
    Result.map f << m

let ofReader (r : Config -> 'a) : Program<'a> =
    Ok << r

let ofResult (r : Result<'a, Error>) : Program<'a> =
    fun _ -> r

let ofOption (msg : Error) (opt : Option<'a>) : Program<'a> =
    fun _ -> FSharpx.Result.ofOption msg opt

let inline withError (msg : Error) (opt : Option<'a>) : Program<'a> =
    ofOption msg opt

let handleError (handler : Error -> 'a) (m : Program<'a>) : Configured<'a> =
    fun conf -> match m conf with | Ok x -> x | Error err -> handler err

let handle (errorHandler : Error -> 'c) (okHandler : 'a -> 'c) (m : Program<'a>) : Configured<'c> =
    fun conf -> match m conf with | Ok x -> okHandler x | Error err -> errorHandler err

let configure : Config -> Program<'a> -> Result<'a, Error> =
    (|>)

let error (err : Error) : Program<'a> =
    fun _ -> Error err

let ok : 'a -> Program<'a> =
    ret



open FSharpx.Operators
    
/// Inject a value into the Reader type
let inline returnM x = returnM program x
/// Sequentially compose two actions, passing any value produced by the first as an argument to the second.
let inline (>>=) m f = bindM program m f
/// Flipped >>=
let inline (=<<) f m = bindM program m f
/// Sequential application
let inline (<*>) f m = applyM program program f m
/// Sequential application
let inline ap m f = f <*> m
/// Transforms a Reader value by using a specified mapping function.
let inline map f m = liftM program f m
/// Infix map
let inline (<!>) f m = map f m
/// Promote a function to a monad/applicative, scanning the monadic/applicative arguments from left to right.
let inline lift2 f a b = returnM f <*> a <*> b
/// Sequence actions, discarding the value of the first argument.
let inline ( *>) x y = lift2 (fun _ z -> z) x y
/// Sequence actions, discarding the value of the second argument.
let inline ( <*) x y = lift2 (fun z _ -> z) x y
/// Sequentially compose two reader actions, discarding any value produced by the first
let inline (>>.) m f = bindM program m (fun _ -> f)
/// Left-to-right Kleisli composition
let inline (>=>) f g = fun x -> f x >>= g
/// Right-to-left Kleisli composition
let inline (<=<) x = flip (>=>) x

let foldM f s = 
    Seq.fold (fun acc t -> acc >>= (flip f) t) (returnM s)

let inline sequence s =
    let inline cons a b = lift2 List.cons a b
    List.foldBack cons s (returnM [])

let inline mapM f x = sequence (List.map f x)

let inline (|@>) m f = map f m

let inline (<@|) f m = map f m
