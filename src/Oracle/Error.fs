module Error



type DataError =
    | InvalidPK
    | InvalidCID
    | InvalidAddress
    | InvalidHash
    | InvalidJson
    | ListTooBig

type ArgumentError =
    | NoArguments
    | ArgumentNotFound of string
    | InvalidArgumentsCombination
    | InvalidFieldName of string
    | InvalidTxWithNoTx
    | TakeTooBig of uint64

type NotFound =
    | Commit
    | Root
    | RootTimestamp

type Error =
    | Argument of ArgumentError
    | API of msg : string
    | Data of DataError
    | FileMustBeRecord
    | NotFound of NotFound
    | EmptyJSON
    | DatabaseDisconnected
    
    
let parseError (error:Error) =
    match error with
    | Argument args ->
        match args with
        | NoArguments -> "No arguments"
        | ArgumentNotFound arg -> sprintf "Argument not found: %s" arg
        | InvalidArgumentsCombination -> "Invalid argument combination"
        | InvalidFieldName field -> sprintf "Invalid field name: \"%s\"" field
        | InvalidTxWithNoTx -> "Can't output transaction hash along with --notx"
        | TakeTooBig maxValue  -> sprintf "Take can't be bigger than %d" maxValue
    | API msg -> msg
    | Data dataError ->
        match dataError with
        | InvalidPK -> "Invalid public key"
        | InvalidCID -> "Invalid contract ID"
        | InvalidAddress -> "Invalid address"
        | InvalidHash -> "Invalid hash"
        | InvalidJson -> "Invalid JSON"
        | ListTooBig  -> "List is too big"
    | FileMustBeRecord-> "File must be a record"
    | NotFound Commit -> "Commit not found"
    | NotFound Root -> "Root not found"
    | NotFound RootTimestamp -> "Root & timestamp pair not found"
    | DatabaseDisconnected -> "Database disconnected"
    | EmptyJSON -> "JSON object can't be empty"