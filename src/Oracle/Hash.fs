module Hash

open Org.BouncyCastle.Crypto.Digests

open Consensus.Hash

open Item
open Record



type Sha3Update<'a> = 'a -> Sha3Digest -> Sha3Digest



module Sha3 =
    
    let empty : Sha3Digest =
        Sha3Digest 256
    
    let finalize (sha3 : Sha3Digest) : Hash =
        let hash = Array.zeroCreate 32
        (Sha3Digest sha3).DoFinal(hash, 0) |> ignore
        Hash hash
    
    
    module update =
        
        let byte : Sha3Update< byte > =
            Primitives.byte.Sha3.update
        
        let sbyte : Sha3Update< sbyte > =
            Primitives.sbyte.Sha3.update
        
        let int16 : Sha3Update< int16 > =
            Primitives.int16.Sha3.update
        
        let uint16 : Sha3Update< uint16 > =
            Primitives.uint16.Sha3.update
        
        let int32 : Sha3Update< int32 > =
            Primitives.int32.Sha3.update
        
        let uint32 : Sha3Update< uint32 > =
            Primitives.uint32.Sha3.update
        
        let int64 : Sha3Update< int64 > =
            Primitives.int64.Sha3.update
        
        let uint64 : Sha3Update< uint64 > =
            Primitives.uint64.Sha3.update
        
        let decimal : Sha3Update< decimal > =
            Primitives.decimal.Sha3.update
        
        let single : Sha3Update< single > =
            Primitives.single.Sha3.update
        
        let double : Sha3Update< double > =
            Primitives.double.Sha3.update
        
        let string : Sha3Update< string > =
            Primitives.string.Sha3.update
        
        let bool : Sha3Update< bool > =
            Primitives.bool.Sha3.update
        
        let Hash : Sha3Update< Hash > =
            Primitives.Hash.Sha3.update
        
        let INumeral : Sha3Update< INumeral > =
            function
            | IByte   x -> x |> byte
            | ISByte  x -> x |> sbyte
            | IInt16  x -> x |> int16
            | IUInt16 x -> x |> uint16
            | IInt32  x -> x |> int32
            | IUInt32 x -> x |> uint32
            | IInt64  x -> x |> int64
            | IUInt64 x -> x |> uint64
            | IDec    x -> x |> decimal
            | ISingle x -> x |> single
            | IDouble x -> x |> double
        
        let rec Item : Sha3Update< Item > =
            function
            | INull ->
                fun _ -> failwith "null.Sha3.update not implemented yet"
            | IBool b ->
                bool b
            | INum num ->
                INumeral num
            | IStr s ->
                string s
            | IRec r ->
                Item <| IArr (Array.concat <| Array.map (fun (key, value) -> [| IStr key ; value |]) r)
            | IArr arr ->
                let folder sha3 value = sha3 |> Item value
                fun digest -> Array.fold folder digest arr



/// The combined hash of the name and the value
let namedItem (name : string) (value : Item) : Hash =
    Sha3.empty
    |> Sha3.update.string name
    |> Sha3.update.Item value
    |> Sha3.finalize

/// For each name - provide the combined hash of the name and the value
let record : Record -> Map<string, Hash> =
    Map.map namedItem

let commit (root : Hash) (timestamp : uint64) : Hash =
    Sha3.empty
    |> Sha3.update.Hash   root
    |> Sha3.update.uint64 timestamp
    |> Sha3.finalize

