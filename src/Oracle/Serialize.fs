module Serialize

open MongoDB.Bson.Serialization
open MongoDB.Bson.Serialization.Serializers
open MBrace.FsPickler

open Item



module Item =
    
    type T = Item
    
    type Serializer() =
        inherit SerializerBase<T>()
        
        let serializer : BinarySerializer =
            FsPickler.CreateBinarySerializer()
        
        override this.Serialize( context : BsonSerializationContext, _ : BsonSerializationArgs , value : T) =
            value |> serializer.Pickle<T> |> context.Writer.WriteBytes
        
        override this.Deserialize( context : BsonDeserializationContext, _ : BsonDeserializationArgs) : T =
            context.Reader.ReadBytes() |> serializer.UnPickle<T>
    
    let register() =
        MongoDB.Bson.Serialization.BsonSerializer.RegisterSerializer( Serializer() )
    


module Hash =

    open Consensus
    
    type T = Hash.Hash
    
    let private fromString encoded =
        match FsBech32.Base16.decode encoded with
        | Some decoded when Array.length decoded = 32
            -> Consensus.Hash.Hash decoded |> Ok
        | _ -> Error "Could not decode hash"
    
    type Serializer() =
        inherit SerializerBase<T>()
        
        override this.Serialize( context : BsonSerializationContext, _ : BsonSerializationArgs , value : T) =
            value |> Hash.toString |> context.Writer.WriteString
        
        override this.Deserialize( context : BsonDeserializationContext, _ : BsonDeserializationArgs) : T =
            match context.Reader.ReadString() |> fromString with
            | Ok x ->
                x
            | Error msg ->
                failwithf "%s" msg
    
    let register() =
        MongoDB.Bson.Serialization.BsonSerializer.RegisterSerializer( Serializer() )



let registerAll() =
    Item.register()
    Hash.register()