module Numeral

open Item



type NumeralMode =
    | ModeByte
    | ModeSByte
    | ModeInt16
    | ModeUInt16
    | ModeInt32
    | ModeUInt32
    | ModeInt64
    | ModeUInt64
    | ModeDec
    | ModeSingle
    | ModeDouble

let convertDecimal : NumeralMode -> decimal -> INumeral =
    function
    | ModeByte   -> IByte   << byte
    | ModeSByte  -> ISByte  << sbyte
    | ModeInt16  -> IInt16  << int16
    | ModeUInt16 -> IUInt16 << uint16
    | ModeInt32  -> IInt32  << int32
    | ModeUInt32 -> IUInt32 << uint32
    | ModeInt64  -> IInt64  << int64
    | ModeUInt64 -> IUInt64 << uint64
    | ModeDec    -> IDec    << id
    | ModeSingle -> ISingle << single
    | ModeDouble -> IDouble << double

let convertFloat : NumeralMode -> float -> INumeral =
    function
    | ModeByte   -> IByte   << byte
    | ModeSByte  -> ISByte  << sbyte
    | ModeInt16  -> IInt16  << int16
    | ModeUInt16 -> IUInt16 << uint16
    | ModeInt32  -> IInt32  << int32
    | ModeUInt32 -> IUInt32 << uint32
    | ModeInt64  -> IInt64  << int64
    | ModeUInt64 -> IUInt64 << uint64
    | ModeDec    -> IDec    << decimal
    | ModeSingle -> ISingle << single
    | ModeDouble -> IDouble << id

let convertString : INumeral -> string =
    function
    | IByte   n -> string n
    | ISByte  n -> string n
    | IInt16  n -> string n
    | IUInt16 n -> string n
    | IInt32  n -> string n
    | IUInt32 n -> string n
    | IInt64  n -> string n
    | IUInt64 n -> string n
    | IDec    n -> string n
    | ISingle n -> string n
    | IDouble n -> string n
