module JSON

open FSharp.Data

open Error
open Item
open Record
open Config
open ProgramMonad

let private bimap f g (x , y) = (f x , g y)


module ParseType =
    
    module Request =
        
        type TimeKeyValueBodyJson =
            JsonProvider<"""
                [[{
                    "key" : "key",
                    "low" : "low"
                }]
                ,
                [{
                    "key"  : "key",
                    "low"  : "low",
                    "high" : "high"
                }]
                ]
            """,SampleIsList=true>

    module Response =

        type CommitJson =
            JsonProvider<"""
            [{
                "root": "root",
                "timestamp": "timestamp",
                "commit": "commit",
                "tx": "tx",
                "messageBody": "message",
                "messageBodyEncoded": "messageBodyEncoded"
            }
            ,
            {
                "root": "root",
                "timestamp": "timestamp",
                "commit": "commit",
                "messageBody": "message",
                "messageBodyEncoded": "messageBodyEncoded"
            }
            ]
            """,SampleIsList=true>


module Item =
    
    let rec fromJson (config : Config) : JsonValue -> Item  =
        function
        | JsonValue.String s ->
            IStr s
        | JsonValue.Number x ->
            INum <| Numeral.convertDecimal config.numeralMode x
        | JsonValue.Float x ->
            INum <| Numeral.convertFloat config.numeralMode x
        | JsonValue.Record r ->
            r
            |> Array.map (bimap id (fromJson config))
            |> IRec
        | JsonValue.Array arr ->
            arr
            |> Array.map (fromJson config)
            |> IArr
        | JsonValue.Boolean b ->
            IBool b
        | JsonValue.Null ->
            INull
    
    let rec toJson : Item -> JsonValue  =
        function
        | IStr s ->
            JsonValue.String s
        | INum x ->
            JsonValue.String (Numeral.convertString x)
        | IRec r ->
            JsonValue.Record (Array.map (bimap id toJson) r)
        | IArr arr ->
            arr
            |> Array.map toJson
            |> JsonValue.Array
        | IBool b ->
            JsonValue.Boolean b
        | INull ->
            JsonValue.Null



module Record =
    
    let fromJsonRecord (config : Config) : (string * JsonValue) [] -> Record  =
        let folder m (key , value) =
            Map.add key (Item.fromJson config value) m
        Array.fold folder Map.empty



let parse (s : string) : Program<Record> = fun config ->
    s
    |> JsonValue.TryParse
    |> function
       | Some (JsonValue.Record r) ->
            Ok <| Record.fromJsonRecord config r
       | Some _ ->
           Error FileMustBeRecord
       | None ->
            Error (Error.Data InvalidJson)
