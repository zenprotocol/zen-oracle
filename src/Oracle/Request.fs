module Request

open FSharp.Data
open Infrastructure
open Result
open Config
open System.Text
open FSharpx.Reader

let result = ResultBuilder<string>()



module Response =
    
    /// Zen Node Api needs to change, until then this will fix
    let getResponse (unformatted : string) : string = 
        unformatted.Substring(1, unformatted.Length - 2)
    
    let getBody (errMessage : string) (request : HttpResponse) : Result<string,string> =
        result {
            let! response =
                Exception.resultWrap<HttpResponse> (fun _ ->  request) errMessage
        
            if response.StatusCode <> 200 then 
                return! Error (sprintf "%s: %d" errMessage response.StatusCode)
            
            return 
                match response.Body with
                | Text text -> getResponse text
                | Binary bytes -> Encoding.ASCII.GetString bytes
        }
    
    let getUri (endpoint : string) : Configured<string> = fun config ->
        sprintf "http://%s/%s" config.envVars.zenUri endpoint
    
    let action (endpoint : string) (json : JsonValue) : Configured<HttpResponse> =
        getUri endpoint
        |> map (json:JsonValue).Request
