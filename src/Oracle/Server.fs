module Html

module OConfig = Config

open FSharp.Data
open FSharpx
open FsNetMQ
open FSharp.Control.Reactive
open MongoDB.Driver
open Logary.Message

open Api
open Infrastructure.Http

open Error
open Config
open ProgramMonad
open Interface

module Actor = FsNetMQ.Actor

module Log = Infrastructure.Log






module Attest =
    
    open Attest
    
    [<Literal>]
    let QUERY_ADDRESS =
        "address"
    [<Literal>]
    let QUERY_PK =
        "pk"
    [<Literal>]
    let QUERY_CID =
        "cid"
    [<Literal>]
    let QUERY_COMMIT =
        "commit"
    [<Literal>]
    let QUERY_TIMESTAMP =
        "timestamp"
    [<Literal>]
    let QUERY_ROOT =
        "root"
    
    let findPK =
        Map.tryFind QUERY_PK
        >> Option.map PK
    
    let findCID =
        Map.tryFind QUERY_CID
        >> Option.map CID
    
    let findAddress =
        Map.tryFind QUERY_ADDRESS
        >> Option.map Address

    
    let parse (db : IMongoDatabase) (query : Map<string, string>) : Program<Return> =
        
        try
            let recipient  =
                None
                |> Option.orElse (findAddress query)
                |> Option.orElse (findPK query)
                |> Option.orElse (findCID query)

            
            match Map.tryFind QUERY_COMMIT query , Map.tryFind QUERY_TIMESTAMP query, Map.tryFind QUERY_ROOT query with
            | Some commit, None, None ->
                eventX "Handling attestation request for commit: {commit}"
                >> setField "commit" commit
                |> Log.info
                Interface.Attest.ByCommit.handle db commit recipient false
            | None, Some timestamp , Some root ->
                eventX "Handling attestation request for timestamp: {commit} and root: {root}"
                >> setField "timestamp" timestamp
                >> setField "root" root
                |> Log.info
                Interface.Attest.ByRootAndTimestamp.handle db root (uint64 timestamp) recipient false
            | _, _, _ ->
                Error (API "Only commit or timestamp and root is needed")
                |> ofResult
        
        with ex ->
            Error (API ex.Message)
            |> ofResult
    
    let getResponseJson (data : Return) : Program<Content> =
        program {
            
            let msg =
                data.messageBody
            
            return
                JSON.ParseType.Response.CommitJson.Root
                    ( data.root
                    , string data.timestamp
                    , data.commit
                    , data.tx
                    , msg.ToString()
                    , data.messageBodyEncoded
                    )
                |> fun json -> json.JsonValue
                |> Api.Helpers.omitNullFields
                |> JsonContent
        }
    
    let handleResult (db:MongoDB.Driver.IMongoDatabase) (query : Map<string, string>) : Program<Content> =
        program {
            let field =
                Map.tryFind "field" query
            
            let! res =
                parse db query
            
            match field with
            | Some field ->
                return!
                    getField field res
                    |> ProgramMonad.map TextContent
            | None ->
                return!
                    getResponseJson res
        }

module GetValues =
    
    open Interface.Query.TimeKeyValue
    
    let parse (db : IMongoDatabase) (body: string) : Program<Return list> =
        try
            handle db (parseJson body)
        with ex ->
           Error (API ex.Message)
           |> ofResult
    
    let generateResponse (rs : Return list) : JsonValue =
        rs
        |> List.map (fun r -> r.toJson)
        |> List.toArray
        |> JsonValue.Array

    let getResponseJson (data : Return list) : Content =
        data
        |> generateResponse
        |> JsonContent
    
    let handleResult (db : IMongoDatabase) (body : string) : Program<Content> =
        parse db body
        |@> getResponseJson

module Query =

    open Query.TimeBounds
    
    let option = FSharpx.Option.option
    
    let handleLogMessage (skip : string option) (take : string option) (low : string option) (high : string option) (key : string option) : unit =
        
        if List.exists (Option.isSome) [low ; high ; key] then
            eventX "Handling query request"
            >> option id (setField "low") low
            >> option id (setField "high") high
            >> option id (setField "key") key
            >> option id (setField "skip") skip
            >> option id (setField "take") take
            |> Log.info
        else
            eventX "Handling empty query request"
            >> option id (setField "skip") skip
            >> option id (setField "take") take
            |> Log.info
    
    let private map = Option.map
    
    let parse (db : IMongoDatabase) (query : Map<string, string>) : Program<Return list> =
        try
            program {
                
                let! config =
                    ask
                
                let low =
                    Map.tryFind "low" query
                
                let high =
                    Map.tryFind "high" query
                
                let key =
                    Map.tryFind "key" query
                
                let skip =
                    Map.tryFind "skip" query
                
                let take =
                    Map.tryFind "take" query
                
                match take with
                | Some take ->
                    if uint64 take > config.querySizeLimit then
                        return!
                            Error <| Argument (TakeTooBig config.querySizeLimit)
                            |> ofResult
                | None ->
                    return!
                        Error <| Argument (ArgumentNotFound "take")
                        |> ofResult
                
                handleLogMessage skip take low high key
                
                return Query.TimeBounds.handle db (map int skip) (map int take) (map uint64 low) (map uint64 high) key
            }
        with ex ->
           Error (API ex.Message)
           |> ofResult

    let private helperJson (ret : Return) : JsonValue =
        let path =
            ret.path
            |> List.map (fun h -> JsonValue.String h.AsString)
            |> List.toArray
        JsonValue.Record
            [|
                ("item", JSON.Item.toJson (Item.IRec [|("key" , Item.IStr ret.name) ; ("value" , ret.value)|]))
                ("timestamp",JsonValue.String (string ret.timestamp))
                ("commit", JsonValue.String (string ret.commitId))
                ("hash", JsonValue.String ret.hash.AsString)
                ("path", JsonValue.Array path)
                ("index", JsonValue.Number ((decimal)ret.index))
                ("root", JsonValue.String ret.root.AsString)
            |]
    
    let getResponseJson (data : Return list) : Content =
        data
        |> List.map helperJson
        |> List.toArray
        |> JsonValue.Array
        |> JsonContent

    let handleResult (db : IMongoDatabase) (query : Map<string, string>) : Program<Content> =
        parse db query
        |@> getResponseJson



module Count =
    
    let option = FSharpx.Option.option
    
    let handleLogMessage (low : string option) (high : string option) (key : string option) : unit =
        
        if List.exists (Option.isSome) [low ; high ; key] then
            eventX "Handling count request"
            >> option id (setField "low") low
            >> option id (setField "high") high
            >> option id (setField "key") key
            |> Log.info
        else
            eventX "Handling empty count request"
            |> Log.info
    
    let private map = Option.map
    
    let parse (db : IMongoDatabase) (query : Map<string, string>) : Program<int64> =
        try
            program {
                
                let low =
                    Map.tryFind "low" query
                
                let high =
                    Map.tryFind "high" query
                
                let key =
                    Map.tryFind "key" query
                
                handleLogMessage low high key
                
                return Query.TimeBoundsCount.handle db (map uint64 low) (map uint64 high) key
            }
        with ex ->
           Error (API ex.Message)
           |> ofResult

    
    let getResponseJson (count : int64) : Content =
        count
        |> decimal
        |> JsonValue.Number
        |> JsonContent

    let handleResult (db : IMongoDatabase) (query : Map<string, string>) : Program<Content> =
        parse db query
        |@> getResponseJson



module Messaging =
    
    let handleReply (reply : ReplyFunction) (content : Result<Content, Error>) : unit =
        match content with
        | Ok (JsonContent x) ->
            reply StatusCode.OK (JsonContent x)
        | Ok (TextContent x) ->
            reply StatusCode.BadRequest (TextContent x)
        | Ok NoContent ->
            reply StatusCode.NoContent NoContent
        | Error err ->
            let status = 
                match err with
                | Argument _ | API _ | Data _ | FileMustBeRecord | EmptyJSON ->
                    StatusCode.BadRequest
                | NotFound _ ->
                    StatusCode.NotFound
                | DatabaseDisconnected ->
                    StatusCode.InternalServerError
            parseError err
            |> TextContent
            |> reply status

    let handleRequest (db : IMongoDatabase) (request : Request) : Program<Content> =
        match request with
        | Get ("/attest", query) ->
            Attest.handleResult db query
        | Get ("/query", query) ->
            Query.handleResult db query
        | Get ("/count", query) ->
            Count.handleResult db query
        | Post ("/getValues", Some body) ->
            GetValues.handleResult db body
        | _ ->
            Error (API "Endpoint not found")
            |> ofResult
    
    let handle (db : IMongoDatabase) (config : OConfig.Config) ((request , reply) : Context) : unit =
        try
            handleRequest db request config
        with ex ->
            eventX "An error has occured: {err}"
            >> setField "err" (string ex)
            |> Log.error
            // TODO: implement this properly
            Error (API ex.Message)
        |> handleReply reply
    
    let handleError error =
        raise error

module InitActor =
    
    open Messaging
    
    let init (config : OConfig.Config) (shim : Socket.T) : unit =
        
        try
        
            use poller =
                Poller.create ()
            
            use observer =
                Poller.registerEndMessage poller shim
            
            use httpAgent =
                Server.create poller config.origin config.envVars.oracleApi
            
            let db =
                DataAccess.connectToDatabase config None
                |> Option.defaultWith (fun () -> failwith "Mongo is not running")
            
            use observer =
                Server.observable httpAgent
                |> Observable.subscribeWithError (handle db config) handleError
            
            Actor.signal shim
            Poller.run poller
        
        with ex ->
            
            eventX (sprintf "%s" ex.Message)
            |> Log.error
            
            raise ex

    
    let create : Configured<Actor.T> =
        init >> Actor.create
