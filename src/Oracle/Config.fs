module Config

open System

open FSharpx.Reader

open Consensus.Chain
open Infrastructure

open Numeral



module Env =
    
    [<Literal>]
    let ZEN_PATH =
        "zen_path"

    [<Literal>]
    let ZEN_NODE_URI =
        "zen_node_uri"

    [<Literal>]
    let ZEN_WALLET_PASSWORD =
        "zen_wallet_password"

    [<Literal>]
    let ORACLE_API =
        "oracle_api"
    
    [<Literal>]
    let MONGO_CONNECTION =
        "mongo_connection"



type EnvVars = {
    derivationPath : string
    zenUri         : string
    walletPass     : string
    oracleApi      : string
    mongoConn      : string option
}

type Config = {
    numeralMode    : NumeralMode
    envVars        : EnvVars
    chain          : Chain
    origin         : Http.Origin
    bodySizeLimit  : uint64
    querySizeLimit : uint64
}

type Configured<'a> = Reader<Config, 'a>

let configured : ReaderBuilder = reader

let configure : Config -> Configured<'a> -> 'a =
    (|>)

let getEnv (key : string) : string = 
    let value = Environment.GetEnvironmentVariable(key)
    if String.IsNullOrEmpty value then
        failwith <| sprintf "missing environment variable: %s" key
    else
        Environment.GetEnvironmentVariable(key)

let tryGetEnv (key : string) : string option =
    let value = Environment.GetEnvironmentVariable(key)
    if String.IsNullOrEmpty value then
        None
    else
        Some <| Environment.GetEnvironmentVariable(key)

let defaultConfig = {
        numeralMode =
            Numeral.ModeUInt64
        envVars = {
            derivationPath = getEnv Env.ZEN_PATH
            zenUri         = getEnv Env.ZEN_NODE_URI
            walletPass     = getEnv Env.ZEN_WALLET_PASSWORD
            oracleApi      = getEnv Env.ORACLE_API
            mongoConn      = tryGetEnv Env.MONGO_CONNECTION
        }
        chain =
            Consensus.Chain.Local
        origin = Http.Origin.No
        bodySizeLimit =
            1000UL
        querySizeLimit =
            1000UL
    }
