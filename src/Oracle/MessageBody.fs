module MessageBody

open FSharpx.Option

open Consensus
open Consensus.Crypto
open Consensus.Hash

module ZData = Zen.Types.Data



module Field =
    
    let COMMIT =
        "Commit"
        |> ZFStar.fsToFstString
    
    let RECIPIENT =
        "Recipient"
        |> ZFStar.fsToFstString
    
    let ORACLE_PUB_KEY =
        "OraclePubKey"
        |> ZFStar.fsToFstString



module Data =
    
    open FSharp.Data
    
    open Zen.Types.Extracted
    open Zen.Types.Data
    open Zen.Dictionary
    
    
    module Dictionary =
        
        let empty : dictionary<data> =
            (Map.empty , 0ul)
        
        let add (key : Prims.string) (value : data) ((m, k) : dictionary<data>) : dictionary<data> =
            (Map.add key value m , k + 1ul)
    
    
    let create (commitHash : Hash) (recipientOpt : lock option) (oraclePubKeyOpt : PublicKey option) : ZData.data =
        Dictionary.empty
        |> Dictionary.add Field.COMMIT (ZData.Hash <| Hash.bytes commitHash) 
        |> option id
            (Dictionary.add Field.ORACLE_PUB_KEY << ZData.PublicKey << ZFStar.fsToFstPublicKey)
            oraclePubKeyOpt
        |> option id
            (Dictionary.add Field.RECIPIENT << ZData.Lock)
            recipientOpt
        |> ZData.Dict
        |> ZData.Collection
    
    
    let toJson chain data =
        Api.Helpers.dataEncoder chain data