module Serialize

let mutable private IS_REGISTERED = false

let registerAll() =
    if not IS_REGISTERED then
        IS_REGISTERED <- true
        Serialize.registerAll()
