module Oracle.Tests.JSON

open System
open FSharp.Data
open FsUnit
open Setup


/// from zenprotocol/Node.Tests: https://gitlab.com/zenprotocol/zenprotocol/-/blob/master/src/Node.Tests/Utils.fs#L48-72
let private uri action = sprintf "http://%s/%s" config.envVars.oracleApi  action

let private check (response:HttpResponse) =
    response.StatusCode
    |> should equal 200
    response

let private getBody (response:HttpResponse) =
    match response.Body with
    | Text string -> string
    | _ -> String.Empty

let get action =
    action
    |> uri
    |> Http.Request
    |> check
    |> getBody

let post action (jsonValue:JsonValue) =
    action
    |> uri
    |> jsonValue.Request
    |> check
    |> getBody
