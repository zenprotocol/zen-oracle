module Merkle


open NUnit.Framework
open FsCheck
open FsCheck.NUnit

open Merkle

open Primitives.Gen
open Primitives


module Gen =
    
    open FSharpx.Prelude
    
    let Merkle : Gen<Merkle> =
        
        let rec Merkle' size =
            match size with
            | 0 ->
                ret Leaf <*| Hash
            | n when n > 0 ->
                [
                    ret Leaf <*| Hash
                    ret (curry3 Node) <*| Hash <*| Merkle' (n - 1) <*| Merkle' (n - 1)
                ]
                |> Gen.oneof
            | _ ->
                invalidArg "size" "Only positive arguments are allowed"
        
        Gen.sized Merkle'
    
    let Proof : Gen<Proof> =
        gen {
            let! n     = int32
            let! index = int32
            let! path  = Gen.listOfLength n Hash
            return {
                index = index
                path  = path
            }
        }
    
    type Arb =
        
        static member Merkle: Arbitrary<Merkle> =
            Arb.fromGen Merkle
        
        static member Proof: Arbitrary<Proof> =
            Arb.fromGen Proof

[<Property(Arbitrary=[| typeof<Primitives.Gen.Arb> |] , MaxTest=1000)>]
let ``reconstructRoot reconstructs the root`` (index : uint8) (hashes0 : NonEmptySet<Hash>) =
    
    let hashes =
        hashes0.Get
        |> Set.toArray
        |> Array.sort
    
    let merkle =
        computeMerkleTree hashes
    
    let paths =
        AuditPath.generate merkle
    
    let index =
        int index % (Array.length paths)
    
    let hash =
        hashes.[index]
    
    let path =
        paths.[index]
    
    let root =
        merkle.hash
    
    AuditPath.reconstructRoot path index hash = root


[<Property(Arbitrary=[| typeof<Primitives.Gen.Arb> |] , MaxTest=500)>]
let ``each element inside a Merkle tree should have a verifiable audit path`` (hashes0 : NonEmptySet<Hash>) =
    
    let hashes =
        hashes0.Get
        |> Set.toArray
    
    let merkle =
        hashes
        |> computeMerkleTree
    
    let paths =
        merkle
        |> AuditPath.generate
    
    let test index =
        let hash = hashes.[index]
        let path = paths.[index]
        AuditPath.reconstructRoot path index hash = merkle.hash
    
    Seq.forall test (seq { 0 .. Array.length hashes - 1 })
