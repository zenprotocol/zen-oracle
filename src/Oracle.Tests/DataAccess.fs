module DataAccess

open NUnit.Framework
open FsCheck
open FsCheck.NUnit

open DataAccess

open Oracle.Tests.Setup
open Primitives.Gen
open Primitives
open Item
open Item.Gen
open Merkle
open Merkle.Gen



module Gen =

    let ArrayProof : Gen<ArrayProof> =
        gen {
            let! index = int32
            let! n     = int32
            let! path  = Gen.arrayOfLength n Hash
            return
                {
                    index = index
                    path  = path
                }
        }

    let private BsonObjectId (seed : int) =
        seed
        |> MongoDB.Bson.ObjectId.GenerateNewId
        |> MongoDB.Bson.BsonObjectId


    module Collections =

        open DataAccess.Collections

        let PublicKey : Gen<PublicKey.T> =
            gen {
                let! seed = int32
                let! pk = string
                return {
                    _id = BsonObjectId seed
                    pk  = pk
                }
            }


        let Item : Gen<Item.T> =
            gen {
                let! seed      = int32
                let! name      = string
                let! root      = Hash
                let! commitId  = Hash
                let! timestamp = uint64
                let! item      = Item
                let! proof     = ArrayProof
                return {
                    _id       = BsonObjectId seed
                    name      = name
                    root      = root
                    commitId  = commitId
                    timestamp = timestamp
                    item      = item
                    proof     = proof
                }
            }


        let Commit : Gen<Commit.T> =
            gen {
                let! seed      = int32
                let! n         = int32
                let! commitId  = Hash
                let! timestamp = uint64
                let! root      = Hash
                let! items     = Gen.arrayOfLength n string
                return {
                    _id       = BsonObjectId seed
                    commitId  = commitId
                    timestamp = timestamp
                    root      = root
                    items     = items
                }
            }


        let TimeRootCommit : Gen<TimeRootCommit.T> =
            gen {
                let! seed      = int32
                let _id        = BsonObjectId
                let! timestamp = uint64
                let! root      = Hash
                let! commitId  = Hash
                return {
                   _id       = BsonObjectId seed
                   timestamp = timestamp
                   root      = root
                   commitId  = commitId
                }
            }

        type Arb =

            static member PublicKey: Arbitrary<PublicKey.T> =
                Arb.fromGen PublicKey

            static member Item: Arbitrary<Item.T> =
                Arb.fromGen Item

            static member Commit: Arbitrary<Commit.T> =
                Arb.fromGen Commit

            static member TimeRootCommit: Arbitrary<TimeRootCommit.T> =
                Arb.fromGen TimeRootCommit

    let namedItemProof : Gen<string * (Item * Proof)> =
        gen {
            let! name = string
            let! item = Item
            let! proof = Proof
            return (name , (item , proof))
        }

    let proofsRecord : Gen<Map<string, Item * Proof>> =
        gen {
            let! n   = uint16
            let! arr = Gen.arrayOfLength (Operators.int32 n + 1) namedItemProof
            return Map.ofArray arr
        }

    let Commit : Gen<Commit.T> =
        gen {
            let! commitId  = Hash
            let! timestamp = uint64
            let! root      = Hash
            let! items     = proofsRecord
            return {
                commitId  = commitId
                timestamp = timestamp
                root      = root
                items     = items
            }
        }

    type Arb =

        static member Commit: Arbitrary<Commit.T> =
            Arb.fromGen Commit



[<TestFixture>]
module Tests =
    
    let config =
        Oracle.Tests.Setup.config

    [<TestFixture>]
    module PublicKey =

        [<OneTimeSetUp>]
        let setup () =
            Serialize.registerAll()
            eprintfn "SETUP..."

        [<TearDown>]
        let teardown () =
            printfn "TEARDOWN..."
            let db = connectToDatabase config dbNameTest |> Option.get
            DataAccess.dropAll db

        [<Property>]
        let ``drop; tryGet = None`` () : bool =
            let db = connectToDatabase config dbNameTest |> Option.get

            Collections.PublicKey.drop db

            Collections.PublicKey.tryGet db = None


        [<Property>]
        let ``put; drop; tryGet = None`` (pk : NonNull<string>) : bool =
            let db = connectToDatabase config dbNameTest |> Option.get

            Collections.PublicKey.put db pk.Get

            Collections.PublicKey.drop db

            Collections.PublicKey.tryGet db = None


        [<Property>]
        let ``put; get; get = put; get`` (pk : NonNull<string>) : bool =
            let db = connectToDatabase config dbNameTest |> Option.get

            Collections.PublicKey.drop db

            Collections.PublicKey.put db pk.Get

            let get1 = Collections.PublicKey.tryGet db

            Collections.PublicKey.drop db

            Collections.PublicKey.put db pk.Get

            Collections.PublicKey.tryGet db |> ignore

            let get2 = Collections.PublicKey.tryGet db

            get1 = get2

[<TestFixture>]
module Commit =
    
    exception UnwantedSuccess of string

    let (|=|) (expected : 'a) (got : 'a) : unit =
        if expected = got then () else failwithf "unexpected - %A; got - %A" expected got

    let H s =
        match FsBech32.Base16.decode s with
        | Some h -> Consensus.Hash.Hash h
        | None -> failwithf "couldn't parse hash - %s" s

    [<OneTimeSetUp>]
    let setup () =
        Serialize.registerAll()
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db
        eprintfn "SETUP..."

    [<TearDown>]
    let teardown () =
        printfn "TEARDOWN..."
        let db = connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db

    [<Test>]
    let ``should fail retrieving uncommitted data`` () : unit =

        let commitId = H "754257c24a166f0e64879407caeb0396cfdb972999f21f60dec86a3eb6eff1c3"
        let timestamp = 1UL
        let root = H "3e47241505bca37f3356fd8dda544c2a3c9c043601f147ea0c6da1362c85a472"

        let db = connectToDatabase config dbNameTest |> Option.get
        
        try
            Collections.Commit.get db commitId
            |> fun x -> sprintf "%A" x |> UnwantedSuccess |> raise   
            
            Collections.TimeRootCommit.get db timestamp root |> ignore
            |> fun x -> sprintf "%A" x |> UnwantedSuccess |> raise
            
            DataAccess.Collections.Item.get db root "APPL" |> ignore
            |> fun x -> sprintf "%A" x |> UnwantedSuccess |> raise
            
        with
        | UnwantedSuccess s ->
            failwithf "got %s - shouldn't get anything" s
        | _ ->
            ()

    [<Test>]
    let ``all the expected data is in the database after a commit`` () : unit =
        let H s =
            match FsBech32.Base16.decode s with
            | Some h -> Consensus.Hash.Hash h
            | None -> failwithf "couldn't parse hash - %s" s

        let commit : Commit.T = {
            commitId = H "754257c24a166f0e64879407caeb0396cfdb972999f21f60dec86a3eb6eff1c3";
            timestamp = 1234UL;
            root = H "3e47241505bca37f3356fd8dda544c2a3c9c043601f147ea0c6da1362c85a472";
            items =
                [("ABCD",
                  (INum (IUInt64 12UL),
                   {index = 0;
                    path =
                     [H "8bedf63712734899064f7c342d38447360ce4d0377cf8b984710b1fd48341a3b";
                      H "0ecb254e1ff36f9b6a09f35926041a01a955171a29d8500775fb58a0acbff54c"];}));
                 ("APPL",
                  (INum (IUInt64 10UL),
                   {index = 1;
                    path =
                     [H "8bedf63712734899064f7c342d38447360ce4d0377cf8b984710b1fd48341a3b";
                      H "9b46882c07f5213e938d57de4c5871b82bff5fc7a04a82c06df1b4c4b9001293"];}));
                 ("WXYZ",
                  (INum (IUInt64 123UL),
                   {index = 2;
                    path =
                     [H "d838d592595dbbf2dfcba67767b644a985f85ebfbd946af4fd04b531001ebc89";
                      H "d8ddbed508169675bc626c925e3531d0795a299c7f3e18b4853e98c3bf5ec87f"];}));
                 ("XYXY",
                  (INum (IUInt64 6456UL),
                   {index = 3;
                    path =
                     [H "d838d592595dbbf2dfcba67767b644a985f85ebfbd946af4fd04b531001ebc89";
                      H "3bedac6f285f43882b35b58894bd40ee6f4f7c1b59440568e86b91944e6447fe"];}))]
                |> Map.ofList
        }

        let db = connectToDatabase config dbNameTest |> Option.get

        Commit.put db commit

        let items =
            commit.items
            |> Map.toArray
            |> Array.sortBy fst

        let res_commit = Collections.Commit.get db commit.commitId

        commit.commitId  |=| res_commit.commitId
        commit.timestamp |=| res_commit.timestamp
        commit.root      |=| res_commit.root

        let res_items =
            items
            |> Array.map fst
            |> Array.map (Collections.Item.get db commit.root)


        for i in [0 .. Array.length items - 1 ] do
            commit.root                                |=| res_items.[i].root
            fst items.[i]                              |=| res_items.[i].name
            (fst << snd) items.[i]                     |=| res_items.[i].item
            ((snd << snd) items.[i]).index             |=| res_items.[i].proof.index
            List.toArray ((snd << snd) items.[i]).path |=| res_items.[i].proof.path


    [<Test>]
    let ``no redundency`` () : unit =

        let commit : Commit.T = {
            commitId = H "754257c24a166f0e64879407caeb0396cfdb972999f21f60dec86a3eb6eff1c3";
            timestamp = 1234UL;
            root = H "3e47241505bca37f3356fd8dda544c2a3c9c043601f147ea0c6da1362c85a472";
            items =
                [("ABCD",
                  (INum (IUInt64 12UL),
                   {index = 0;
                    path =
                     [H "8bedf63712734899064f7c342d38447360ce4d0377cf8b984710b1fd48341a3b";
                      H "0ecb254e1ff36f9b6a09f35926041a01a955171a29d8500775fb58a0acbff54c"];}));
                 ("APPL",
                  (INum (IUInt64 10UL),
                   {index = 1;
                    path =
                     [H "8bedf63712734899064f7c342d38447360ce4d0377cf8b984710b1fd48341a3b";
                      H "9b46882c07f5213e938d57de4c5871b82bff5fc7a04a82c06df1b4c4b9001293"];}));
                 ("WXYZ",
                  (INum (IUInt64 123UL),
                   {index = 2;
                    path =
                     [H "d838d592595dbbf2dfcba67767b644a985f85ebfbd946af4fd04b531001ebc89";
                      H "d8ddbed508169675bc626c925e3531d0795a299c7f3e18b4853e98c3bf5ec87f"];}));
                 ("XYXY",
                  (INum (IUInt64 6456UL),
                   {index = 3;
                    path =
                     [H "d838d592595dbbf2dfcba67767b644a985f85ebfbd946af4fd04b531001ebc89";
                      H "3bedac6f285f43882b35b58894bd40ee6f4f7c1b59440568e86b91944e6447fe"];}))]
                |> Map.ofList
        }

        let db = connectToDatabase config dbNameTest |> Option.get

        eprintfn "%A" commit

        Commit.put db commit

        let res1 = Collections.Commit.get db commit.commitId

        Commit.put db commit

        let res2 = Collections.Commit.get db commit.commitId



        if res1 <> res2 then
            failwith "fail"
