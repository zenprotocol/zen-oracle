module Numeral

open FsCheck

open Primitives.Gen

open Numeral



module Gen =
    
    let NumeralMode : Gen<NumeralMode> =
        [
            ret ModeByte
            ret ModeSByte
            ret ModeInt16
            ret ModeUInt16
            ret ModeInt32
            ret ModeUInt32
            ret ModeInt64
            ret ModeUInt64
            ret ModeDec
            ret ModeSingle
            ret ModeDouble
        ]
        |> Gen.oneof
    
    
    type Arb =
        
        static member NumeralMode: Arbitrary<NumeralMode> =
            Arb.fromGen NumeralMode
