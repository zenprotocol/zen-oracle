module Primitives

open Consensus

open FsCheck



module Gen =
    
    let (<*|) = Gen.apply
    
    let (<@|) = Gen.map
    
    let ret = Gen.constant
    let byte    = Arb.generate<byte>
    let sbyte   = Arb.generate<sbyte>
    let int16   = Arb.generate<int16>
    let uint16  = Arb.generate<uint16>
    let int32   = Arb.generate<int32>
    let uint32  = Arb.generate<uint32>
    let int64   = Arb.generate<int64>
    let uint64  = Arb.generate<uint64>
    let decimal = Arb.generate<decimal>
    let single  = Arb.generate<single>
    let double  = Arb.generate<double>
    let string  = Arb.generate<string>
    let bool    = Arb.generate<bool>
    
    let Hash : Gen<Hash.Hash> =
        gen {
            let! n = int32
            let! arr = Gen.arrayOfLength n Arb.generate<byte>
            return Hash.compute arr
        }

    type Arb =
        
        static member Hash : Arbitrary<Hash.Hash> =
            Arb.fromGen Hash
