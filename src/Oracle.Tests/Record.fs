module Record

open FsCheck

open Item

open Primitives.Gen



module Gen =
    
    let namedItem : Gen<string * Item> =
        gen {
            let! name = string
            let! item = Gen.Item
            return (name , item)
        }
    
    let Record : Gen<Map<string, Item>> =
        gen {
            let! n = int32
            let! arr = Gen.arrayOfLength n namedItem
            return Map.ofArray arr
        }
    
    
    type Arb =
        
        static member namedItem: Arbitrary<string * Item> =
            Arb.fromGen namedItem
        
        static member Record : Arbitrary<Map<string, Item>> =
            Arb.fromGen Record


