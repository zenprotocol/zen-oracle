module Error

open FsCheck

open Error

open Primitives.Gen



module Gen =
    
    let DataError : Gen<DataError> =
        [
            ret InvalidPK
            ret InvalidCID
            ret InvalidJson
        ]
        |> Gen.oneof
    
    let ArgumentError : Gen<ArgumentError> =
        [
            ret NoArguments
            ret ArgumentNotFound <*| string
        ]
        |> Gen.oneof
    
    let Error : Gen<Error> =
        [
            ret Argument <*| ArgumentError
            ret API <*| string
            ret Data <*| DataError
            ret FileMustBeRecord
        ]
        |> Gen.oneof
    
    type Arb =
        
        static member DataError : Arbitrary<DataError> =
            Arb.fromGen DataError
        
        static member ArgumentError : Arbitrary<ArgumentError> =
            Arb.fromGen ArgumentError
        
        static member Error : Arbitrary<Error> =
            Arb.fromGen Error
