module Item

open FsCheck

open Item

open Primitives.Gen
open Primitives



module Gen =
    
    open FSharpx.Prelude
    
    let INumeral : Gen<INumeral> =
        [
            ret IByte   <*| byte
            ret ISByte  <*| sbyte
            ret IInt16  <*| int16
            ret IUInt16 <*| uint16
            ret IInt32  <*| int32
            ret IUInt32 <*| uint32
            ret IInt64  <*| int64
            ret IUInt64 <*| uint64
            ret IDec    <*| decimal
            ret ISingle <*| single
            ret IDouble <*| double
        ]
        |> Gen.oneof

    let Item : Gen<Item> =
        
        let rec Item' size : Gen<Item> =
            match size with
            | 0 ->
                [
                    ret INull
                    ret IBool <*| bool
                    ret INum  <*| INumeral
                    ret IStr  <*| string
                ]
                |> Gen.oneof
            | n when n > 0 ->
                [
                    ret INull
                    ret IBool <*| bool
                    ret INum  <*| INumeral
                    ret IStr  <*| string
                    //ret (curry IRec) <*| string <*| (Item' (n - 1))
                    gen {
                        let! n = int32
                        let! arr = Gen.arrayOfLength n (Item' (n - 1))
                        return IArr arr
                    }
                ]
                |> Gen.oneof
            | _ ->
                Item' 0
                // invalidArg "size" "Only positive arguments are allowed"
        
        Gen.sized Item'
    
    
    type Arb =
        
        static member INumeral: Arbitrary<INumeral> =
            Arb.fromGen INumeral
        
        static member Item : Arbitrary<Item> =
            Arb.fromGen Item
