module Oracle.Tests.Interface.Interface

open NUnit.Framework

open Oracle.Tests.Setup

open Error
open Item
open Merkle

open Interface


let config = Oracle.Tests.Setup.config

module Examples =

    let H s =
        match FsBech32.Base16.decode s with
        | Some h -> Consensus.Hash.Hash h
        | None -> failwithf "couldn't parse hash - %s" s

    let pubkey1 =
        "02ad784974b3f86ad97e008e20d2c107429041ed2d991ada2a1461b5077c11944c"

    let commit1 : DataAccess.Commit.T = {
        commitId =  H "754257c24a166f0e64879407caeb0396cfdb972999f21f60dec86a3eb6eff1c3";
        timestamp = 1234UL;
        root = H "3e47241505bca37f3356fd8dda544c2a3c9c043601f147ea0c6da1362c85a472";
        items =
            [("ABCD", (INum (IUInt64 12UL),
                {index = 0; path =
                [H "8bedf63712734899064f7c342d38447360ce4d0377cf8b984710b1fd48341a3b";
                H "0ecb254e1ff36f9b6a09f35926041a01a955171a29d8500775fb58a0acbff54c"];}));
            ("APPL", (INum (IUInt64 10UL),
                {index = 1; path =
                [H "8bedf63712734899064f7c342d38447360ce4d0377cf8b984710b1fd48341a3b";
                H "9b46882c07f5213e938d57de4c5871b82bff5fc7a04a82c06df1b4c4b9001293"];}));
            ("WXYZ", (INum (IUInt64 123UL),
                {index = 2; path =
                [H "d838d592595dbbf2dfcba67767b644a985f85ebfbd946af4fd04b531001ebc89";
                H "d8ddbed508169675bc626c925e3531d0795a299c7f3e18b4853e98c3bf5ec87f"];}));
            ("XYXY", (INum (IUInt64 6456UL),
                {index = 3; path =
                [H "d838d592595dbbf2dfcba67767b644a985f85ebfbd946af4fd04b531001ebc89";
                H "3bedac6f285f43882b35b58894bd40ee6f4f7c1b59440568e86b91944e6447fe"];}))]
            |> Map.ofList
        }



[<TestFixture>]
module Attest =

    [<OneTimeSetUp>]
    let onetimesetup () =
        Serialize.registerAll()
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db

    [<OneTimeTearDown>]
    let onetimeteardown () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db

    [<SetUp>]
    let setup () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db
        DataAccess.Collections.PublicKey.put db Examples.pubkey1
        DataAccess.Commit.put db Examples.commit1

    [<TearDown>]
    let teardown () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db

    [<Test>]
    let ``valid commit id`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let commitId = Examples.commit1.commitId.AsString

        match Attest.ByCommit.handle db commitId None false config with
        | Ok _ -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``commit id doesn't exist`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let commitId = "754257c24a166f0e64879407caeb0396cfdb972999f21f60dec86a3eb6eff1c4"

        match Attest.ByCommit.handle db commitId None false config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (NotFound Commit) -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``commit id too long`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let commitId = let x = Examples.commit1.commitId.AsString in x + x + x

        match Attest.ByCommit.handle db commitId None false config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (Data InvalidHash) -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``commit id too short`` () =

        let _ =
            let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
            let commitId = let x = Examples.commit1.commitId.AsString in x.Remove (x.Length / 2)

            match Attest.ByCommit.handle db commitId None false config with
            | Ok _ -> failwithf "Invalid operation didn't fail"
            | Error (Data InvalidHash) -> ()
            | Error err -> failwithf "ERROR: %A" err

        let _ =
            let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
            let commitId = let x = Examples.commit1.commitId.AsString in x.Remove (x.Length - 1)

            match Attest.ByCommit.handle db commitId None false config with
            | Ok _ -> failwithf "Invalid operation didn't fail"
            | Error (Data InvalidHash) -> ()
            | Error err -> failwithf "ERROR: %A" err
        ()


    [<Test>]
    let ``commit id empty`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let commitId = ""

        match Attest.ByCommit.handle db commitId None false config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (Data InvalidHash) -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``valid root and timestamp`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let root = Examples.commit1.root.AsString
        let timestamp = Examples.commit1.timestamp

        match Attest.ByRootAndTimestamp.handle db root timestamp None false config with
        | Ok _ -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``root doesn't exist`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let root = "3e47241505bca37f3356fd8dda544c2a3c9c043601f147ea0c6da1362c85a473"
        let timestamp = Examples.commit1.timestamp

        match Attest.ByRootAndTimestamp.handle db root timestamp None false config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (NotFound RootTimestamp) -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``timestamp doesn't exist`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let root = Examples.commit1.root.AsString
        let timestamp = 131313UL

        match Attest.ByRootAndTimestamp.handle db root timestamp None false config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (NotFound RootTimestamp) -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``root is too long`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let root = let x = Examples.commit1.root.AsString in x + x
        let timestamp = Examples.commit1.timestamp

        match Attest.ByRootAndTimestamp.handle db root timestamp None false config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (Data InvalidHash) -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``root is too short`` () =

        let _ =
            let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
            let root = let x = Examples.commit1.root.AsString in x.Remove (x.Length / 2)
            let timestamp = Examples.commit1.timestamp

            match Attest.ByRootAndTimestamp.handle db root timestamp None false config with
            | Ok _ -> failwithf "Invalid operation didn't fail"
            | Error (Data InvalidHash) -> ()
            | Error err -> failwithf "ERROR: %A" err

        let _ =
            let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
            let root = let x = Examples.commit1.root.AsString in x.Remove (x.Length - 1)
            let timestamp = Examples.commit1.timestamp

            match Attest.ByRootAndTimestamp.handle db root timestamp None false config with
            | Ok _ -> failwithf "Invalid operation didn't fail"
            | Error (Data InvalidHash) -> ()
            | Error err -> failwithf "ERROR: %A" err

        ()

    [<Test>]
    let ``root is empty`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let root = ""
        let timestamp = Examples.commit1.timestamp

        match Attest.ByRootAndTimestamp.handle db root timestamp None false config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (Data InvalidHash) -> ()
        | Error err -> failwithf "ERROR: %A" err



[<TestFixture>]
module AuditPath =

    let items1 =
        Examples.commit1.items
        |> Map.map (fun _ -> fst)

    [<OneTimeSetUp>]
    let onetimesetup () =
        Serialize.registerAll()
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db

    [<OneTimeTearDown>]
    let onetimeteardown () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db

    [<SetUp>]
    let setup () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.Collections.PublicKey.put db Examples.pubkey1
        DataAccess.Commit.put db Examples.commit1

    [<TearDown>]
    let teardown () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db

    [<Test>]
    let ``valid commit id and all items`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let commitId = Examples.commit1.commitId.AsString
        match Audit.ByCommit.handle db commitId items1 config with
        | Ok res ->
            if res.root = Examples.commit1.root.AsString then
                let resproofs =
                    res.proofs
                    |> List.map (fun proof -> (proof.name, { index = proof.index ; path = proof.path }))
                    |> List.sortBy fst

                let origproofs =
                    Examples.commit1.items
                    |> Map.toList
                    |> List.sortBy fst
                    |> List.map (fun (name, (_ , proof)) -> (name , proof))

                if resproofs <> origproofs then
                    failwithf "ERROR: audit paths aren't the same as expected audit paths"
            else
                failwithf "ERROR: root isn't the expected root"
        | Error err ->
            failwithf "ERROR: %A" err

    [<Test>]
    let ``valid commit id and some items`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let commitId = Examples.commit1.commitId.AsString
        let items =
            items1
            |> Map.toList
            |> List.sortBy fst
            |> List.take 2
            |> Map.ofList
        match Audit.ByCommit.handle db commitId items config with
        | Ok res ->
            if res.root = Examples.commit1.root.AsString then
                let resproofs =
                    res.proofs
                    |> List.map (fun proof -> (proof.name, { index = proof.index ; path = proof.path }))
                    |> List.sortBy fst

                let origproofs =
                    Examples.commit1.items
                    |> Map.toList
                    |> List.sortBy fst
                    |> List.take 2
                    |> List.map (fun (name, (_ , proof)) -> (name , proof))

                if resproofs <> origproofs then
                    failwithf "ERROR: audit paths aren't the same as expected audit paths"
            else
                failwithf "ERROR: root isn't the expected root"
        | Error err ->
            failwithf "ERROR: %A" err

    [<Test>]
    let ``invalid commit id`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let commitId = let x = Examples.commit1.commitId.AsString in x.Remove (x.Length / 2)
        match Audit.ByCommit.handle db commitId items1 config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (Data InvalidHash) -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``commit id doesn't exist`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let commitId = "754257c24a166f0e64879407caeb0396cfdb972999f21f60dec86a3eb6eff1c4"
        match Audit.ByCommit.handle db commitId items1 config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (NotFound Commit) -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``valid commit id and no items`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let commitId = Examples.commit1.commitId.AsString
        match Audit.ByCommit.handle db commitId Map.empty config with
        | Ok res ->
            if res.root = Examples.commit1.root.AsString then
                if res.proofs <> [] then
                    failwith "ERROR: unexpected proofs"
            else
                failwithf "ERROR: root isn't the expected root"
        | Error err ->
            failwithf "ERROR: %A" err

    [<Test>]
    let ``valid root and items`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let root = Examples.commit1.root.AsString
        match Audit.ByRoot.handle db root items1 config with
        | Ok res ->
            if res.root = Examples.commit1.root.AsString then
                let resproofs =
                    res.proofs
                    |> List.map (fun proof -> (proof.name, { index = proof.index ; path = proof.path }))
                    |> List.sortBy fst

                let origproofs =
                    Examples.commit1.items
                    |> Map.toList
                    |> List.sortBy fst
                    |> List.map (fun (name, (_ , proof)) -> (name , proof))

                if resproofs <> origproofs then
                    failwithf "ERROR: audit paths aren't the same as expected audit paths"
            else
                failwithf "ERROR: root isn't the expected root"
        | Error err ->
            failwithf "ERROR: %A" err

    [<Test>]
    let ``invalid root`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let root = let x = Examples.commit1.root.AsString in x.Remove (x.Length / 2)
        match Audit.ByRoot.handle db root items1 config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (Data InvalidHash) -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``root doesn't exist`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let root = "3e47241505bca37f3356fd8dda544c2a3c9c043601f147ea0c6da1362c85a473"
        match Audit.ByRoot.handle db root items1 config with
        | Ok _ -> failwithf "Invalid operation didn't fail"
        | Error (NotFound Root) -> ()
        | Error err -> failwithf "ERROR: %A" err

    [<Test>]
    let ``valid root and no items`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let root = Examples.commit1.root.AsString
        match Audit.ByRoot.handle db root Map.empty config with
        | Ok res ->
            if res.root = root then
                if res.proofs <> [] then
                    failwith "ERROR: unexpected proofs"
            else
                failwithf "ERROR: root isn't the expected root"
        | Error err ->
            failwithf "ERROR: %A" err



[<TestFixture>]
module Query =

    let items1 =
        Examples.commit1.items
        |> Map.map (fun _ -> fst)

    [<OneTimeSetUp>]
    let onetimesetup () =
        Serialize.registerAll()
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db

    [<OneTimeTearDown>]
    let onetimeteardown () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db

    [<SetUp>]
    let setup () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.Collections.PublicKey.put db Examples.pubkey1
        DataAccess.Commit.put db Examples.commit1

    [<TearDown>]
    let teardown () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        DataAccess.dropAll db

    [<Test>]
    let ``valid timebounds query - all fields`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let low = 1UL
        let high = 54321UL
        let name = "APPL"
        match Query.TimeBounds.handle db None None (Some low) (Some high) (Some name) with
        | res :: [] ->
            if res.name <> name then
                failwithf "invalid name - expected %A, got %A" res.name name
            if res.timestamp <> Examples.commit1.timestamp then
                failwithf "invalid timestamp - expected %A, got %A" res.timestamp Examples.commit1.timestamp
            if res.value <> Map.find name items1 then
                failwithf "invalid value - expected %A, got %A" res.value (Map.find name items1)
            if res.commitId <> Examples.commit1.commitId then
                failwithf "invalid commit id - expected %A, got %A" res.commitId Examples.commit1.commitId
            if res.path <> (Map.find name Examples.commit1.items |> snd).path then
                failwithf "invalid path - expected %A, got %A" res.path (Map.find name Examples.commit1.items |> snd).path
        | [] ->
            failwith "query not found"
        | _ :: _ :: _ ->
            failwith "too many results"

    [<Test>]
    let ``valid timebounds query - no low`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let high = 54321UL
        let name = "APPL"
        match Query.TimeBounds.handle db None None None (Some high) (Some name) with
        | res :: [] ->
            if res.name <> name then
                failwithf "invalid name - expected %A, got %A" res.name name
            if res.timestamp <> Examples.commit1.timestamp then
                failwithf "invalid timestamp - expected %A, got %A" res.timestamp Examples.commit1.timestamp
            if res.value <> Map.find name items1 then
                failwithf "invalid value - expected %A, got %A" res.value (Map.find name items1)
            if res.commitId <> Examples.commit1.commitId then
                failwithf "invalid commit id - expected %A, got %A" res.commitId Examples.commit1.commitId
        | [] ->
            failwith "query not found"
        | _ :: _ :: _ ->
            failwith "too many results"

    [<Test>]
    let ``valid timebounds query - no high`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let low = 1UL
        let name = "APPL"
        let proof = snd <| Map.find name Examples.commit1.items
        match Query.TimeBounds.handle db None None (Some low) None (Some name) with
        | res :: [] ->
            if res.name <> name then
                failwithf "invalid name - expected %A, got %A" res.name name
            if res.timestamp <> Examples.commit1.timestamp then
                failwithf "invalid timestamp - expected %A, got %A" res.timestamp Examples.commit1.timestamp
            if res.value <> Map.find name items1 then
                failwithf "invalid value - expected %A, got %A" res.value (Map.find name items1)
            if res.commitId <> Examples.commit1.commitId then
                failwithf "invalid commit id - expected %A, got %A" res.commitId Examples.commit1.commitId
            if res.path <> proof.path then
                failwithf "invalid path - expected %A, got %A" res.path proof.path
            if res.index <> proof.index then
                failwithf "invalid index - expected %A, got %A" res.index proof.index
        | [] ->
            failwith "query not found"
        | _ :: _ :: _ ->
            failwith "too many results"

    [<Test>]
    let ``valid timebounds query - no name. check that all items are accounted for`` () =
        let db = DataAccess.connectToDatabase config dbNameTest |> Option.get
        let low = 1UL
        let high = 54321UL
        let results = Query.TimeBounds.handle db None None (Some low) (Some high) None

        let resnames =
            results
            |> List.map (fun res -> res.name)
            |> List.sort

        let orignames =
            Examples.commit1.items
            |> Map.toList
            |> List.map fst
            |> List.sort

        if orignames <> resnames then
            failwith "not all names are accounted for"
