module Oracle.Tests.Setup


open FsNetMQ
open FSharp.Data
open Api.Types
open FsUnit
open Consensus
open Infrastructure

module Actor = FsNetMQ.Actor

let busName = "test"

let dbNameTest =
    Some "oracletest"

let config : Config.Config = {
        numeralMode =
            Numeral.ModeUInt64
        envVars = {
            derivationPath =
                "m/44'/258'/0'/0/0"
            zenUri =
                "127.0.0.1:31567"
            walletPass =
                "1234"
            oracleApi =
                "*:8586"
            mongoConn =
                None
        }
        chain = Chain.Local
        origin = Http.Origin.No
        bodySizeLimit = 1000UL
        querySizeLimit = 1000UL
    }

let createBroker () =
     Actor.create (fun shim ->
        use poller = Poller.create ()
        use emObserver = Poller.registerEndMessage poller shim

        use sbBroker = ServiceBus.Broker.create poller busName None
        use evBroker = EventBus.Broker.create poller busName None

        Actor.signal shim
        Poller.run poller
    )
    
let getActors () =
    [
        createBroker ()
        Html.InitActor.create config
    ]
    |> List.rev
    |> List.map Disposables.toDisposable
    |> Disposables.fromList
