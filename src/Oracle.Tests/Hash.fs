module Hash

open NUnit.Framework
open FsCheck
open FsCheck.NUnit

open Hash

open Zen.Cost.Realized
open Consensus

open Infrastructure
open Item

[<Property>]
let ``hashing of named integers should yield the same result of the SHA3 hashing scheme in Zulib``
    ( name  : NonNull<string> )
    ( value : uint64          )
    : bool
    =
    
    let zulibHash =
        Zen.Hash.Sha3.empty
        |> Zen.Hash.Sha3.updateString (ZFStar.fsToFstString name.Get) |> __force 
        |> Zen.Hash.Sha3.updateU64 value                              |> __force
        |> Zen.Hash.Sha3.finalize                                     |> __force
        |> Hash.Hash
    
    let oracleHash =
        namedItem name.Get (INum <| INumeral.IUInt64 value)
    
    oracleHash = zulibHash
