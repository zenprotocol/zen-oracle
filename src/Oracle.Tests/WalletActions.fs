module WalletActions

open FsCheck

open WalletActions

open Primitives.Gen



module Gen =
    
    
    module Execute =
        
        let Env : Gen<Execute.Env> =
            gen {
                let! address       = string
                let! command       = string
                let! messageBody   = string
                let! returnAddress = bool
                return {
                    address       = address
                    command       = command
                    messageBody   = messageBody
                    returnAddress = returnAddress
                }
            }
